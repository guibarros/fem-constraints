%exemplo
close all
clear
clc

nodes = fem.node.array(4);
nodes(1) = fem.analysis.frame.Node( 0,  0);
nodes(2) = fem.analysis.frame.Node( 0, -3);
nodes(3) = fem.analysis.frame.Node(-6,  0);
nodes(4) = fem.analysis.frame.Node( 0,  2);

x = 1;
y = 2;
theta = 3;
q = 4;
nodes(1).setForces([y,theta],3*q*[-1,1]);
nodes(2).setDisplacements([x,y,theta],[0,0,0]);
nodes(3).setDisplacements([x,y,theta],[0,0,0]);
nodes(4).setDisplacements([x,y,theta],[0,0,0]);

beta = 1;
EA = 3e6;
EI = 3e5;
prop = fem.analysis.frame.NavierProp(beta*EA,EI);

elements = fem.element.array(3);
elements(1) = fem.analysis.frame.Navier(nodes([2,1]),prop);
elements(2) = fem.analysis.frame.Navier(nodes([3,1]),prop);
elements(3) = fem.analysis.frame.Navier(nodes([1,4]),prop);

A = [1,0,0];
b = 0;
for e=1:length(elements)
    elements(e) = fem.constraint.strain.Colocation(elements(e),A,b,0);    
end

% fea = fem.Analysis;
fea = fem.Constraints;
fea.Nodes = nodes;
fea.Elements = elements;
fea.Run();

disp(nodes(1).Displacement)

pos = fem.analysis.frame.Pos1D(nodes,elements);
figure
pos.Scale = 0;
plot(pos,'k');
hold on
pos.Scale = .5;
plot(pos,'-b');
hold off
axis equal
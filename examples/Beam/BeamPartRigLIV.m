close all
clear
clc

P = 1;
EA = 1e8;
EI = 1e5;
L = 1;

nodes = fem.node.array(3);
nodes(1) = fem.analysis.frame.Node(  0, 0);
nodes(2) = fem.analysis.frame.Node(  L, 0);
nodes(3) = fem.analysis.frame.Node(2*L, 0);

x = 1;
y = 2;
theta = 3;
nodes(1).setDisplacements([x,y],[0,0]);
nodes(3).setDisplacements([x,y,theta],[0,0,0]);

prop = fem.analysis.frame.NavierProp(EA,EI);

a = .10;
% A = [1,0,0;0,a,L-a;0,1,-1];
% b = [0;1;0];
A = [1,0,0;0,1,0;0,0,1];
b = [1;0;0];
elements = fem.element.array(2);
elements(1) = fem.analysis.frame.Navier(nodes(1:2),prop);
elements(1) = fem.constraint.strain.Colocation(elements(1),A,b,0);
elements(2) = fem.analysis.frame.Navier(nodes(2:3),prop);

fea = fem.Constraints;
fea.Nodes = nodes;
fea.Elements = elements;
fea.Run();

pos = fem.analysis.frame.Pos1D(nodes,elements);
pos.Scale = 0;
plot(pos,'k');
hold on
pos.Scale = .5;
plot(pos,'--b');
hold off
axis equal
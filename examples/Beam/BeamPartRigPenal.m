L = 1; EI = 10^5; P = 1;
K = @(B) 3*EI/L^2*[(4+B)/L (2-B); (2-B) (4/3+B)*L];
F = [P;0];
d = @(B) K(B)\F;
M = @(B) 2*EI/L^2*[3, 2*L]*d(B);
y = @(B) M(B)/(10*P*L/28);
X = [10^12:10^12:10^13,2*10^13:10^13:10^14,2*10^14:10^14:10^15,2*10^15:10^15:6*10^15];
N = length(X);
Y = zeros(N,1);
for i=1:N
    Y(i) = y(X(i));
end
h = semilogx(X,Y);
set(h,'Color','black');
set(gca,'TickLabelInterpreter','latex')
xlabel('$\beta$','Interpreter','latex')
ylabel('$\frac{\mathcal{M}\left(L\right)}{10PL/28}$','Interpreter','latex')
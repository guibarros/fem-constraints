close all
clear
clc
%% Input
E = 200e3;   %Young's modulus [N/mm2]
nu = 0.;     %Poisson ratio
t = 10;      %Thickness [mm]
l = 10;      %Geometry Parameter [mm]
P = 10e3;    %Load [N]
a = 0;     %Mesh distortion [mm]
ny = 10;
nx = (5+mod(ny,2))*ny;
%% Properties
prop = fem.analysis.plane.ElasticProp(E,nu,t);
constitutive = fem.analysis.plane.Strain();
%% Mesh Generator
[coords, connect] = fem.util.mesh([10*l, 2*l],[nx,ny], 'quadrilateral', 'quad');

nodes = fem.node.array(length(coords));
for n=1:length(coords)
    nodes(n) = fem.analysis.plane.Node(coords(n, 1), coords(n, 2));
end
elements = fem.element.array(length(connect));
for m=1:length(connect)
    elements(m) = fem.analysis.plane.Q8(nodes(connect(m,:)),prop,constitutive);
end

A = [0,1,0;
     0,0,1];
b = [0;0];
for e=1:length(elements)
    int = elements(e).Integration;
    [xg,yg] = int.IntegrationPoints(4);
    psi1 = @(xi)+3/4*(xg(1)+xi(1))*(yg(1)+xi(2));
    psi2 = @(xi)-3/4*(xg(2)+xi(1))*(yg(2)+xi(2));
    psi3 = @(xi)-3/4*(xg(3)+xi(1))*(yg(3)+xi(2));
    psi4 = @(xi)+3/4*(xg(4)+xi(1))*(yg(4)+xi(2));
    psi = @(xi)[psi1(xi), 0, psi2(xi), 0, psi3(xi), 0, psi4(xi), 0;...
                0, psi1(xi), 0, psi2(xi), 0, psi3(xi), 0, psi4(xi)];
    elements(e) = fem.constraint.strain.Integral(elements(e),A,b,psi);
end

% A = [0,1,0];
% b = 0;
% for e=1:nx*ny
%     int = elements(e).Integration;
%     [xg,yg] = int.IntegrationPoints(4);
%     psi1 = @(xi)+3/4*(xg(1)+xi(1))*(yg(1)+xi(2));
%     psi2 = @(xi)-3/4*(xg(2)+xi(1))*(yg(2)+xi(2));
%     psi3 = @(xi)-3/4*(xg(3)+xi(1))*(yg(3)+xi(2));
%     psi4 = @(xi)+3/4*(xg(4)+xi(1))*(yg(4)+xi(2));
%     psi = @(xi)[psi1(xi), psi2(xi), psi3(xi), psi4(xi)];
%     elements(e) = fem.constraint.strain.Integral(elements(e),A,b,psi);
% end
%% Boundary Condiction
x = 1;
y = 2;
nodes(ny/2+1).setDisplacements([x,y],[0,0]);
nodes(nx*(ny+1)+ny/2+1).setDisplacements([x,y],[0,0]);
nodes((nx/2+1)*(ny+1)).setForces(2,-P);
%% Analysis
fea = fem.Constraints;
% fea = fem.Analysis;
fea.Nodes = nodes;
fea.Elements = elements;
fea.Run();
%% Visualization

pos = fem.analysis.plane.Pos2D(nodes,elements);

figure
axis('equal','tight','off')
pos.Scale = .0;
plot(pos,'FaceColor','none');
pos.Scale = .5;
plot(pos,'FaceColor',[.8,.8,.8],'FaceAlpha',.8);

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'stress';
pos.PlotAxis = 'xx';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'stress';
pos.PlotAxis = 'yy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'stress';
pos.PlotAxis = 'xy';
pos.Scale = .5;
pos.maxValue= 2;
pos.minValue=-10;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');
pos.maxValue= Inf;
pos.minValue=-Inf;

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'strain';
pos.PlotAxis = 'xx';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'strain';
pos.PlotAxis = 'yy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'strain';
pos.PlotAxis = 'xy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');
close all
clear
clc

P = 10;
E = 200;
v = 0.3;
t = 10;
l = 10;
a = 0;

nodes(1) = fem.analysis.plane.Node( 10*l,  l);
nodes(2) = fem.analysis.plane.Node(    0, -l);
nodes(3) = fem.analysis.plane.Node(5*l+a, -l);
nodes(4) = fem.analysis.plane.Node( 10*l, -l);
nodes(5) = fem.analysis.plane.Node(5*l-a,  l);
nodes(6) = fem.analysis.plane.Node(    0,  l);

x = 1;
y = 2;
nodes(2).setDisplacements([x,y],[0,0]);
nodes(6).setDisplacements(x,0);
nodes(1).setForces(x,P/2);
nodes(4).setForces(x,P/2);

prop = fem.analysis.plane.ElasticProp(E,v,t);
constitutive = fem.analysis.plane.Stress();

elements(1) = fem.analysis.plane.Q4(nodes([2,3,5,6]),prop,constitutive);
elements(2) = fem.analysis.plane.Q4(nodes([3,4,1,5]),prop,constitutive);

fea = fem.Analysis;
fea.Nodes = nodes;
fea.Elements = elements;
fea.Run();

result = table;
result.u = [P*10*l/E/(2*l)/t; (nodes(1).Displacement(1)+nodes(4).Displacement(1))*.5];
result.v = [-v*2*l*P/E/(2*l)/t; nodes(1).Displacement(2)];
result.Properties.RowNames={'Analytic','Numeric'};
disp(result);

pos = fem.analysis.plane.Pos2D(nodes,elements);

figure
axis('equal','tight','off')
pos.Scale = .0;
plot(pos,'FaceColor','none');
pos.Scale = .5;
plot(pos,'FaceColor',[.8,.8,.8],'FaceAlpha',.8);

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'stress';
pos.PlotAxis = 'xx';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'stress';
pos.PlotAxis = 'yy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'stress';
pos.PlotAxis = 'xy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'strain';
pos.PlotAxis = 'xx';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'strain';
pos.PlotAxis = 'yy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'strain';
pos.PlotAxis = 'xy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');
close all
clear
clc
%% Input
E = 200e3;   %Young's modulus [N/mm2]
nu = 0.4999; %Poisson ratio
t = 10;      %Thickness [mm]
l = 10;      %Geometry Parameter [mm]
P = 10e3;    %Load [N]
M = P*2*l;   %Moment [Nmm]
a = 0;       %Mesh distortion [mm]
ny = 5;
nx = (5+mod(ny,2))*ny;
%% Properties
prop = fem.analysis.plane.ElasticProp(E,nu,t);
constitutive = fem.analysis.plane.Strain();
%% Mesh Generator
dx = 10*l/nx; %Width of the element
dy = 2*l/ny;  %Heigth of the element
%Nodes
nodes = fem.node.array((nx+1)*(ny+1));
n = 0;
for i=1:nx/2
    for j=1:(ny+1)
        n = n + 1;
        x = (i-1)*dx;
        y = (j-1)*dy-l;
        x = x - a*x*y/5/l/l;
        nodes(n) = fem.analysis.plane.Node(x,y);
    end
end
for i=nx/2+1:nx+1
    for j=1:(ny+1)
        n = n + 1;
        x = (i-1)*dx;
        y = (j-1)*dy-l;
        x = x - y/l*a*(2-x/5/l);
        nodes(n) = fem.analysis.plane.Node(x,y);
    end
end
elements = fem.element.array(nx*ny);
m = 0;
for i=1:nx
    for j=1:ny
        a = j + (ny+1)*(i-1);
        b = j + (ny+1)*i;
        c = (j+1) + (ny+1)*i;
        d = (j+1) + (ny+1)*(i-1);
        m = m + 1;
        elements(m) = fem.analysis.plane.Q4(nodes([a,b,c,d]),prop,constitutive);
    end
end
A = [1,1,0];
b = 0;
for e=1:nx*ny
    int = elements(e).Integration;
    [xg,yg] = int.IntegrationPoints(int.NumPoints);
    shape_func = @(xi)3/4*[
        (xg(1)+xi(1))*(yg(1)+xi(2)),...
        -1*(xg(2)+xi(1))*(yg(2)+xi(2)),...
        -1*(xg(3)+xi(1))*(yg(3)+xi(2)),...
        (xg(4)+xi(1))*(yg(4)+xi(2))];
    elements(e) = fem.constraint.strain.Integral(elements(e),A,b,shape_func);
end
%% Boundary Condiction
x = 1;
y = 2;
nodes(1).setDisplacements([x,y],[0,0]);
for i = 2:ny+1
    nodes(i).setDisplacements(x,0);
end
% i = 1:ny+1;
% y = (i-1)*dy-l;
% p = -M/sum(y.*y);
% for i = 1:ny+1
%     y = (i-1)*dy-l;
%     nodes((ny+1)*nx+i).setForces(x,p*y);
% end
Q = zeros(ny+1,2);
for i = 1:ny
    Q(i,  1) = (ny+1)*nx+i;
    Q(i+1,1) = (ny+1)*nx+i+1;
    y = [(i-1)*dy-l;i*dy-l];
    p = dy/6*[2,1;1,2]*(-12*M/8/l^3)*y;
    Q(i  ,2) = Q(i  ,2) + p(1);
    Q(i+1,2) = Q(i+1,2) + p(2);
end
x = 1;
for i = 1:ny+1
    nodes(Q(i,1)).setForces(x,Q(i,2));
end
%% Analysis
fea = fem.Constraints;
% fea = fem.Analysis;
fea.Nodes = nodes;
fea.Elements = elements;
fea.Run();
%% Visualization
nu = .5;
result = table;
x = nodes((nx+1)*(ny+1)).Coords(1);
y = nodes((nx+1)*(ny+1)).Coords(2);
result.u = [-3*P*(1-nu^2)*x*y/(E*l^2)/t; nodes((nx+1)*(ny+1)).Displacement(1)];
result.v = [3*P*(1-nu^2)/(2*E)*((x/l)^2+nu/(1-nu)*((y/l)^2-1))/t; nodes((nx+1)*(ny+1)).Displacement(2)];
result.Properties.RowNames={'Analytic','Numeric'};
disp(result);

pos = fem.analysis.plane.Pos2D(nodes,elements);

figure
axis('equal','tight','off')
pos.Scale = .0;
plot(pos,'FaceColor','none');
pos.Scale = .5;
plot(pos,'FaceColor',[.8,.8,.8],'FaceAlpha',.8);

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'stress';
pos.PlotAxis = 'xx';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'stress';
pos.PlotAxis = 'yy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'stress';
pos.PlotAxis = 'xy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'strain';
pos.PlotAxis = 'xx';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'strain';
pos.PlotAxis = 'yy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'strain';
pos.PlotAxis = 'xy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');
close all
clear
% clc
%% Input
E = 200e3;   %Young's modulus [N/mm2]
nu = 0.4999;    %Poisson ratio
t = 10;      %Thickness [mm]
l = 10;      %Geometry Parameter [mm]
P = 10e3;    %Load [N]
M = P*2*l;   %Moment [Nmm]
a = 0;       %Mesh distortion [mm]
ny = 6;
nx = (5+mod(ny,2))*ny;
%% Properties
prop = fem.analysis.plane.ElasticProp(E,nu,t);
constitutive = fem.analysis.plane.Strain();
%% Mesh Generator
dx = 10*l/nx; %Width of the element
dy = 2*l/ny;  %Heigth of the element
%Nodes
nodes = fem.node.array((nx+1)*(ny+1));
n = 0;
for i=1:nx/2
    for j=1:(ny+1)
        n = n + 1;
        x = (i-1)*dx;
        y = (j-1)*dy-l;
        x = x - a*x*y/5/l/l;
        nodes(n) = fem.analysis.plane.Node(x,y);
    end
end
for i=nx/2+1:nx+1
    for j=1:(ny+1)
        n = n + 1;
        x = (i-1)*dx;
        y = (j-1)*dy-l;
        x = x - y/l*a*(2-x/5/l);
        nodes(n) = fem.analysis.plane.Node(x,y);
    end
end
elements = fem.element.array(nx*ny);
m = 0;
for i=1:nx
    for j=1:ny
        a = j + (ny+1)*(i-1);
        b = j + (ny+1)*i;
        c = (j+1) + (ny+1)*i;
        d = (j+1) + (ny+1)*(i-1);
        m = m + 1;
        elements(m) = fem.analysis.plane.Q4(nodes([a,b,c,d]),prop,constitutive);
    end
end
A = [1,1,0];
b = 0;
for e=1:nx*ny
        elements(e) = fem.constraint.strain.Integral(elements(e),A,b);
end
%% Boundary Condiction
x = 1;
y = 2;
nodes(1).setDisplacements([x,y],[0,0]);
for i = 2:ny+1
    nodes(i).setDisplacements(x,0);
end
i = 1:ny+1;
y = (i-1)*dy-l;
p = -M/sum(y.*y);
for i = 1:ny+1
    y = (i-1)*dy-l;
    nodes((ny+1)*nx+i).setForces(x,p*y);
end
%% Analysis
fea = fem.Constraints;
fea.Nodes = nodes;
fea.Elements = elements;
nu_array = .4:1e-4:.5;
errorAnal = nu_array;
i=0;
for nu = nu_array
    prop.v = nu;
    fea.Run();
    x = nodes((nx+1)*(ny+1)).Coords(1);
    y = nodes((nx+1)*(ny+1)).Coords(2);
    anal = 3*P*(1-.5^2)/(2*E)*((x/l)^2+.5/(1-.5)*((y/l)^2-1))/t;
    num = nodes((nx+1)*(ny+1)).Displacement(2);
    i=i+1;
    errorAnal(i) = abs(anal-num)/anal;
end
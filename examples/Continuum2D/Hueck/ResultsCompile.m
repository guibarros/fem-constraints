addpath('.\examples\Continuum2D\Hueck\Q4');
addpath('.\examples\Continuum2D\Hueck\T3');
load('Q4_nu0.mat')
figure
plot(numElem,topDisp,'k')
hold on
plot(numElem,numElem*0+Analytic,'--k')
hold off
xlabel('Number of Q4-elements','Interp','latex')
ylabel('Tip deflection $\delta_A$','Interp','latex')
set(gca,'TickLabelInterpreter','latex')
title(sprintf('Q4 - $\\nu = %g$',nu),'Interp','latex')

load('Q4_nu03.mat')
figure
plot(numElem,topDisp,'k')
hold on
plot(numElem,numElem*0+Analytic,'--k')
hold off
xlabel('Number of Q4-elements','Interp','latex')
ylabel('Tip deflection $\delta_A$','Interp','latex')
set(gca,'TickLabelInterpreter','latex')
title(sprintf('Q4 - $\\nu = %g$',nu),'Interp','latex')

load('Q4_nu04999.mat')
figure
plot(numElem,topDisp,'k')
hold on
plot(numElem,numElem*0+Analytic,'--k')
hold off
xlabel('Number of Q4-elements','Interp','latex')
ylabel('Tip deflection $\delta_A$','Interp','latex')
set(gca,'TickLabelInterpreter','latex')
title(sprintf('Q4 - $\\nu = %g$',nu),'Interp','latex')

load('Q4_nuVAR.mat')
figure
plot(nu_array,errorAnal,'k')
xlabel('$\nu$','Interp','latex')
ylabel('Error of tip deflection $\delta_A$','Interp','latex')
set(gca,'TickLabelInterpreter','latex')
title(sprintf('$%g$ Q4-elements',numElem),'Interp','latex')

load('Q4_nu0_incomp.mat')
figure
plot(numElem,topDisp,'k')
hold on
plot(numElem,numElem*0+Analytic,'--k')
hold off
xlabel('Number of Q4-elements','Interp','latex')
ylabel('Tip deflection $\delta_A$','Interp','latex')
set(gca,'TickLabelInterpreter','latex')
title(sprintf('Constrained Q4 - $\\nu = %g$',nu),'Interp','latex')

load('Q4_nu03_incomp.mat')
figure
plot(numElem,topDisp,'k')
hold on
plot(numElem,numElem*0+Analytic,'--k')
hold off
xlabel('Number of Q4-elements','Interp','latex')
ylabel('Tip deflection $\delta_A$','Interp','latex')
set(gca,'TickLabelInterpreter','latex')
title(sprintf('Constrained Q4 - $\\nu = %g$',nu),'Interp','latex')

load('Q4_nu04999_incomp.mat')
figure
plot(numElem,topDisp,'k')
hold on
plot(numElem,numElem*0+Analytic,'--k')
hold off
xlabel('Number of Q4-elements','Interp','latex')
ylabel('Tip deflection $\delta_A$','Interp','latex')
set(gca,'TickLabelInterpreter','latex')
title(sprintf('Constrained Q4 - $\\nu = %g$',nu),'Interp','latex')

load('Q4_nuVAR_incomp.mat')
figure
plot(nu_array,errorAnal,'k')
xlabel('$\nu$','Interp','latex')
ylabel('Error of tip deflection $\delta_A$','Interp','latex')
set(gca,'TickLabelInterpreter','latex')
title(sprintf('$%g$ constrained Q4-elements',numElem),'Interp','latex')

load('T3_nu0.mat')
figure
plot(numElem,topDisp,'k')
hold on
plot(numElem,numElem*0+Analytic,'--k')
hold off
xlabel('Number of T3-elements','Interp','latex')
ylabel('Tip deflection $\delta_A$','Interp','latex')
set(gca,'TickLabelInterpreter','latex')
title(sprintf('T3 - $\\nu = %g$',nu),'Interp','latex')

load('T3_nu03.mat')
figure
plot(numElem,topDisp,'k')
hold on
plot(numElem,numElem*0+Analytic,'--k')
hold off
xlabel('Number of T3-elements','Interp','latex')
ylabel('Tip deflection $\delta_A$','Interp','latex')
set(gca,'TickLabelInterpreter','latex')
title(sprintf('T3 - $\\nu = %g$',nu),'Interp','latex')

load('T3_nu04999.mat')
figure
plot(numElem,topDisp,'k')
hold on
plot(numElem,numElem*0+Analytic,'--k')
hold off
xlabel('Number of T3-elements','Interp','latex')
ylabel('Tip deflection $\delta_A$','Interp','latex')
set(gca,'TickLabelInterpreter','latex')
title(sprintf('T3 - $\\nu = %g$',nu),'Interp','latex')

load('T3_nuVAR.mat')
figure
plot(nu_array,errorAnal,'k')
xlabel('$\nu$','Interp','latex')
ylabel('Error of tip deflection $\delta_A$','Interp','latex')
set(gca,'TickLabelInterpreter','latex')
title(sprintf('$%g$ T3-elements',numElem),'Interp','latex')

load('T3_nu0_incomp.mat')
figure
plot(numElem,topDisp,'k')
hold on
plot(numElem,numElem*0+Analytic,'--k')
hold off
xlabel('Number of T3-elements','Interp','latex')
ylabel('Tip deflection $\delta_A$','Interp','latex')
set(gca,'TickLabelInterpreter','latex')
title(sprintf('Constrained T3 - $\\nu = %g$',nu),'Interp','latex')

load('T3_nu03_incomp.mat')
figure
plot(numElem,topDisp,'k')
hold on
plot(numElem,numElem*0+Analytic,'--k')
hold off
xlabel('Number of T3-elements','Interp','latex')
ylabel('Tip deflection $\delta_A$','Interp','latex')
set(gca,'TickLabelInterpreter','latex')
title(sprintf('Constrained T3 - $\\nu = %g$',nu),'Interp','latex')

load('T3_nu04999_incomp.mat')
figure
plot(numElem,topDisp,'k')
hold on
plot(numElem,numElem*0+Analytic,'--k')
hold off
xlabel('Number of T3-elements','Interp','latex')
ylabel('Tip deflection $\delta_A$','Interp','latex')
set(gca,'TickLabelInterpreter','latex')
title(sprintf('Constrained T3 - $\\nu = %g$',nu),'Interp','latex')

load('T3_nuVAR_incomp.mat')
figure
plot(nu_array,errorAnal,'k')
xlabel('$\nu$','Interp','latex')
ylabel('Error of tip deflection $\delta_A$','Interp','latex')
set(gca,'TickLabelInterpreter','latex')
title(sprintf('$%g$ T3-elements',numElem),'Interp','latex')
close all
clear
clc
%% Input
E = 200e3;   %Young's modulus [N/mm2]
nu = 0.0; %Poisson ratio
t = 10;      %Thickness [mm]
l = 10;      %Geometry Parameter [mm]
P = 10e3;    %Load [N]
M = P*2*l;   %Moment [Nmm]
a = 0;     %Mesh distortion [mm]
ny = 4;
nx = (5+mod(ny,2))*ny;
%% Properties
prop = fem.analysis.plane.ElasticProp(E,nu,t);
constitutive = fem.analysis.plane.Strain();
%% Mesh Generator
dx = 10*l/nx; %Width of the element
dy = 2*l/ny;  %Heigth of the element
%Nodes
nodes = fem.node.array((nx+1)*(ny+1)+ny*(nx+1)+nx*(ny+1));
n = 0;
for i=1:nx/2
    for j=1:(ny+1)
        n = n + 1;
        x = (i-1)*dx;
        y = (j-1)*dy-l;
        x = x - a*x*y/5/l/l;
        nodes(n) = fem.analysis.plane.Node(x,y);
    end
end
for i=nx/2+1:nx+1
    for j=1:(ny+1)
        n = n + 1;
        x = (i-1)*dx;
        y = (j-1)*dy-l;
        x = x - y/l*a*(2-x/5/l);
        nodes(n) = fem.analysis.plane.Node(x,y);
    end
end
for i=1:nx/2
    for j=1:ny
        n = n + 1;
        x = (i-1)*dx;
        y = (j-1)*dy+dy/2-l;
        x = x - a*x*y/5/l/l;
        nodes(n) = fem.analysis.plane.Node(x,y);
    end
end
for i=nx/2+1:nx+1
    for j=1:ny
        n = n + 1;
        x = (i-1)*dx;
        y = (j-1)*dy+dy/2-l;
        x = x - y/l*a*(2-x/5/l);
        nodes(n) = fem.analysis.plane.Node(x,y);
    end
end
for i=1:nx/2
    for j=1:(ny+1)
        n = n + 1;
        x = (i-1)*dx+dx/2;
        y = (j-1)*dy-l;
        x = x - a*x*y/5/l/l;
        nodes(n) = fem.analysis.plane.Node(x,y);
    end
end
for i=nx/2+1:nx
    for j=1:(ny+1)
        n = n + 1;
        x = (i-1)*dx+dx/2;
        y = (j-1)*dy-l;
        x = x - y/l*a*(2-x/5/l);
        nodes(n) = fem.analysis.plane.Node(x,y);
    end
end
elements = fem.element.array(nx*ny);
m = 0;
for i=1:nx
    for j=1:ny
        a = j + (ny+1)*(i-1);
        b = j + (ny+1)*i;
        c = (j+1) + (ny+1)*i;
        d = (j+1) + (ny+1)*(i-1);
        e = (nx+1)*(ny+1) + ny*(nx+1) + (ny+1)*(i-1) + j;
        f = (nx+1)*(ny+1) + ny*(i) + j;
        g = (nx+1)*(ny+1) + ny*(nx+1) + (ny+1)*(i-1) + j + 1;
        h = (nx+1)*(ny+1) + ny*(i-1) + j;
        m = m + 1;
        elements(m) = fem.analysis.plane.Q8(nodes([a,b,c,d,e,f,g,h]),prop,constitutive);
    end
end

A = [1,1,0];
b = 0;
for e=1:nx*ny
    int = elements(e).Integration;
    [xg,yg] = int.IntegrationPoints(4);
    psi1 = @(xi)+3/4*(xg(1)+xi(1))*(yg(1)+xi(2));
    psi2 = @(xi)-3/4*(xg(2)+xi(1))*(yg(2)+xi(2));
    psi3 = @(xi)-3/4*(xg(3)+xi(1))*(yg(3)+xi(2));
    psi4 = @(xi)+3/4*(xg(4)+xi(1))*(yg(4)+xi(2));
    psi = @(xi)[psi1(xi), psi2(xi), psi3(xi), psi4(xi)];
    elements(e) = fem.constraint.strain.Integral(elements(e),A,b,psi);
end
%% Boundary Condiction
x = 1;
y = 2;
nodes(1).setDisplacements([x,y],[0,0]);
for i = 2:ny+1
    nodes(i).setDisplacements(x,0);
end
for i = 1:ny
    nodes((nx+1)*(ny+1) + i).setDisplacements(x,0);
end
Q = zeros(2*ny+1,2);
for i = 1:ny
    Q(2*i-1,1) = (ny+1)*nx+i;
    Q(2*i  ,1) = (nx+1)*(ny+1)+ny*nx+i;
    Q(2*i+1,1) = (ny+1)*nx+i + 1;
    y = [(i-1)*dy-l;(i-1)*dy+dy/2-l;i*dy-l];
    p = dy/30*[4,2,-1;2,16,2;-1,2,4]*(-12*M/8/l^3)*y;
    Q(2*i-1,2) = Q(2*i-1,2) + p(1);
    Q(2*i  ,2) = Q(2*i  ,2) + p(2);
    Q(2*i+1,2) = Q(2*i+1,2) + p(3);    
end
x = 1;
for i = 1:2*ny+1
    nodes(Q(i,1)).setForces(x,Q(i,2));
end
% nodes((nx+1)*(ny+1)).setForces(2,P);
%% Analysis
fea = fem.Constraints;
% fea = fem.Analysis;
fea.Nodes = nodes;
fea.Elements = elements;
fea.Run();
%% Visualization
nu = .5;
result = table;
x = nodes((nx+1)*(ny+1)).Coords(1);
y = nodes((nx+1)*(ny+1)).Coords(2);
result.u = [-3*P*(1-nu^2)*x*y/(E*l^2)/t; nodes((nx+1)*(ny+1)).Displacement(1)];
result.v = [3*P*(1-nu^2)/(2*E)*((x/l)^2+nu/(1-nu)*((y/l)^2-1))/t; nodes((nx+1)*(ny+1)).Displacement(2)];
result.Properties.RowNames={'Analytic','Numeric'};
disp(result);

pos = fem.analysis.plane.Pos2D(nodes,elements);

figure
axis('equal','tight','off')
pos.Scale = .0;
plot(pos,'FaceColor','none');
pos.Scale = .5;
plot(pos,'FaceColor',[.8,.8,.8],'FaceAlpha',.8);

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'stress';
pos.PlotAxis = 'xx';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'stress';
pos.PlotAxis = 'yy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'stress';
pos.PlotAxis = 'xy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'strain';
pos.PlotAxis = 'xx';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'strain';
pos.PlotAxis = 'yy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'strain';
pos.PlotAxis = 'xy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');
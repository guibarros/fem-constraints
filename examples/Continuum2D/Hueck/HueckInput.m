close all
clear
clc

P = 10e3;
E = 200e3;
v = 0.0;
t = 10;
l = 10;
a = 0;

nodes(1) = fem.analysis.plane.Node( 10*l,  l);
nodes(2) = fem.analysis.plane.Node(    0, -l);
nodes(3) = fem.analysis.plane.Node(5*l+a, -l);
nodes(4) = fem.analysis.plane.Node( 10*l, -l);
nodes(5) = fem.analysis.plane.Node(5*l-a,  l);
nodes(6) = fem.analysis.plane.Node(    0,  l);

x = 1;
y = 2;
nodes(2).setDisplacements([x,y],[0,0]);
nodes(6).setDisplacements(x,0);
nodes(1).setForces(x,-P);
nodes(4).setForces(x, P);

prop = fem.analysis.plane.ElasticProp(E,v,t);
constitutive = fem.analysis.plane.Strain();

elements(1) = fem.analysis.plane.Q4(nodes([2,3,5,6]),prop,constitutive);
elements(2) = fem.analysis.plane.Q4(nodes([3,4,1,5]),prop,constitutive);
A = [1,1,0];
b = 0;
for e=1:2
%     int = elements(e).Integration;
%     [xg,yg] = int.IntegrationPoints(int.NumPoints);
%     elements(e) = fem.constraint.strain.Colocation(elements(e),A,b,[xg',yg']);
    elements(e) = fem.constraint.strain.Integral(elements(e),A,b);
end

fea = fem.Constraints;
fea.Nodes = nodes;
fea.Elements = elements;
fea.Run();

pos = fem.analysis.plane.Pos2D(nodes,elements);

figure
axis('equal','tight','off')
pos.Scale = .0;
plot(pos,'FaceColor','none');
pos.Scale = .5;
plot(pos,'FaceColor',[.8,.8,.8],'FaceAlpha',.8);

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'stress';
pos.PlotAxis = 'xx';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'stress';
pos.PlotAxis = 'yy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'stress';
pos.PlotAxis = 'xy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'strain';
pos.PlotAxis = 'xx';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'strain';
pos.PlotAxis = 'yy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'strain';
pos.PlotAxis = 'xy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');
close all
clear
clc
%% Input
Lx = 4500;        %Length [mm]
Ly = 450;         %Heigth [mm]
t  = 200;         %Thickness [mm]
E = 200e3;        %Young's modulus [N/mm2]
nu = 0.3;      %Poisson ratio
q = 2.5;          %Distributed Load [N/mm2]
nx = 20;           %Number of elements in axis x
ny = 2;           %Number of elements in axis y

%% Properties
prop = fem.analysis.plane.ElasticProp(E,nu,t);
constitutive = fem.analysis.plane.Stress();

%% Mesh Generator
dx = Lx/nx;                         %Length of the element
dy = Ly/ny;                         %Heigth of the element
%Nodes
nodes = fem.node.array((nx+1)*(ny+1)+nx*ny);
n = 0;
for i=1:(nx+1)
    for j=1:(ny+1)
        n = n + 1;
        nodes(n) = fem.analysis.plane.Node((i-1)*dx,(j-1)*dy);
    end
end
for i=1:(nx)
    for j=1:(ny)
        n = n + 1;
        nodes(n) = fem.analysis.plane.Node((i-1)*dx+dx/2,(j-1)*dy+dy/2);
    end
end

elements = fem.element.array(4*nx*ny);
m = 0;
for i=1:nx
    for j=1:ny
        a = j + (ny+1)*(i-1);
        b = j + (ny+1)*i;
        c = (j+1) + (ny+1)*i;
        d = (j+1) + (ny+1)*(i-1);
        e = (nx+1)*(ny+1)+m/4+1;
        m = m + 1;
        elements(m) = fem.analysis.plane.T3(nodes([a,b,e]),prop,constitutive);
        m = m + 1;
        elements(m) = fem.analysis.plane.T3(nodes([b,c,e]),prop,constitutive);
        m = m + 1;
        elements(m) = fem.analysis.plane.T3(nodes([c,d,e]),prop,constitutive);
        m = m + 1;
        elements(m) = fem.analysis.plane.T3(nodes([d,a,e]),prop,constitutive);
    end
end

A = [0,1,0];
b = 0;
for e=1:ny*4:nx*ny*4
    for i=1:ny*4
        elements(e+i-1) = fem.constraint.strain.Colocation(elements(e+i-1),A,b,0);
    end
end

%% Boundary Condiction
x = 1;
y = 2;
nodes(ny/2+1).setDisplacements([x,y],[0,0]);
nodes(nx*(ny+1)+ny/2+1).setDisplacements([x,y],[0,0]);
for i=1:nx
    j = (ny+1)*i;
    k = (ny+1)*(i+1);
    fj = nodes(j).Force;
    nodes(j).setForces(y,fj(2)-q*dx*t/2);
    fk = nodes(k).Force;
    nodes(k).setForces(y,fk(2)-q*dx*t/2);
end
%% Analysis
fea = fem.Constraints;
fea.Nodes = nodes;
fea.Elements = elements;
fea.Run();
%% Visualization
pos = fem.analysis.plane.Pos2D(nodes,elements);

figure
axis('equal','tight','off')
pos.Scale = .0;
plot(pos,'FaceColor','none');
pos.Scale = .5;
plot(pos,'FaceColor',[.8,.8,.8],'FaceAlpha',.8);

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'stress';
pos.PlotAxis = 'xx';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'stress';
pos.PlotAxis = 'yy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'stress';
pos.PlotAxis = 'xy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'strain';
pos.PlotAxis = 'xx';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'strain';
pos.PlotAxis = 'yy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');

figure
axis('equal','tight','off')
pos.PlotType = 'mesh';
pos.Scale = .0;
plot(pos,'FaceColor','none');
colormap jet
pos.PlotType = 'strain';
pos.PlotAxis = 'xy';
pos.Scale = .5;
plot(pos,'EdgeAlpha',.2);
colorbar('southoutside');
close all
clear
% clc
E = 200;     %Young's modulus [kN/mm2]
nu = 0.4999;    %Poisson ratio
t = 10;      %Thickness [mm]
G = E/(1+nu)/2;
prop = fem.analysis.plane.ElasticProp(E,nu,t);
constitutive = fem.analysis.plane.Strain();

nodes(1) = fem.analysis.plane.Node(0,0);
nodes(2) = fem.analysis.plane.Node(2,0);
nodes(3) = fem.analysis.plane.Node(2,1);
nodes(4) = fem.analysis.plane.Node(0,1);
nodes(1).id = 1;
nodes(2).id = 2;
nodes(3).id = 3;
nodes(4).id = 4;
elements(1) = fem.analysis.plane.Q4(nodes(1:4),prop,constitutive);
elements(2) = fem.analysis.plane.QM6(nodes(1:4),prop,constitutive);

for el=elements
    K = el.Stiffness();
    [v,k] = eig((K+K')/2);
    for i=1:8
        for j=1:4
            nodes(j).setDisplacements([1,2],v(2*j-1:2*j,i));
        end
        pos = fem.analysis.plane.Pos2D(nodes,el);
        figure
        axis('equal','tight','off')
        pos.Scale = .0;
        plot(pos,'FaceColor','none');
        pos.Scale = .5;
        plot(pos,'FaceColor',[.8,.8,.8],'FaceAlpha',.8);
        title([class(el),' ',num2str(k(i,i)/G/t)])
    end
end
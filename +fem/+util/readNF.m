function [nodes, elements, props, constitutive] = readNF( file_name )

num_elem_t3 = 0;
num_elem_t6 = 0;
num_elem_q4 = 0;
num_elem_q8 = 0;
num_elem_load = 0;
num_nodal_load = 0;

fileID = fopen(file_name,'r');
while ~feof(fileID)
    str = fgetl(fileID);
    if isempty(str)
        continue;
    end
    if strcmp(str,'%HEADER.ANALYSIS')
        analysis_type = fscanf(fileID,'%s',1);
        continue;
    end
    if strcmp(str,'%NODE')
        num_nodes = fscanf(fileID,'%d',1);
        continue;
    end
    if strcmp(str,'%NODE.COORD')
        fgetl(fileID);
        coords = fscanf(fileID,'%d %f %f %f',[4 num_nodes]);
        continue;
    end
    if strcmp(str,'%NODE.SUPPORT')
        num_supps = fscanf(fileID,'%d',1);
        supps = fscanf(fileID,'%d %d %d %d %d %d %d',[7 num_supps]);
        continue;
    end
    if strcmp(str,'%MATERIAL.ISOTROPIC')
        num_mats = fscanf(fileID,'%d',1);
        mats = fscanf(fileID,'%d %f %f',[3 num_mats]);
        continue;
    end
    if strcmp(str,'%THICKNESS')
        num_thicks = fscanf(fileID,'%d',1);
        thicks = fscanf(fileID,'%d %f',[2 num_thicks]);
        continue;
    end
    if strcmp(str,'%ELEMENT')
        num_elem = fscanf(fileID,'%d',1);
        continue;
    end
    if strcmp(str,'%ELEMENT.T3')
        num_elem_t3 = fscanf(fileID,'%d',1);
        elem_t3 = fscanf(fileID,'%d',[7 num_elem_q8]);
        continue;
    end
    if strcmp(str,'%ELEMENT.T6')
        num_elem_t6 = fscanf(fileID,'%d',1);
        elem_t6 = fscanf(fileID,'%d',[10 num_elem_q8]);
        continue;
    end
    if strcmp(str,'%ELEMENT.Q4')
        num_elem_q4 = fscanf(fileID,'%d',1);
        elem_q4 = fscanf(fileID,'%d',[8 num_elem_q8]);
        continue;
    end
    if strcmp(str,'%ELEMENT.Q8')
        num_elem_q8 = fscanf(fileID,'%d',1);
        elem_q8 = fscanf(fileID,'%d',[12 num_elem_q8]);
        continue;
    end
    if strcmp(str,'%LOAD.CASE.LINE.FORCE.UNIFORM')
        num_elem_load = fscanf(fileID,'%d',1);
        elem_load = fscanf(fileID,'%d %d %d %d %f %f %f',[7 num_elem_load]);
        continue;
    end
    if strcmp(str,'%LOAD.CASE.NODAL.FORCES')
        num_nodal_load = fscanf(fileID,'%d',1);
        nodal_load = fscanf(fileID,'%d %f %f %f %f %f %f',[7 num_nodal_load]);
        continue;
    end    
end
fclose(fileID);

if strcmp(analysis_type,'''plane_stress''')
    constitutive = fem.analysis.plane.Stress();
elseif strcmp(analysis_type,'''plane_strain''')
    constitutive = fem.analysis.plane.Strain();
end

nodes = fem.node.array(num_nodes);

for i=1:num_nodes
    nodes(coords(1,i)) = fem.analysis.plane.Node( coords(2,i), coords(3,i) );
    nodes(coords(1,i)).id = coords(1,i);
end

for i=1:num_supps
    dir = 1:6;
    dir = dir(logical(supps(2:end,i)));
    nodes(supps(1,i)).setDisplacements(dir,0*dir);
end

props(num_mats,num_thicks) = fem.element.property.Base;
for i = 1:num_mats
    E = mats(2,i);
    v = mats(3,i);
    for j = 1:num_thicks
        t = thicks(2,j);
        props(i,j) = fem.analysis.plane.ElasticProp(E,v,t);
    end
end

elements = fem.element.array(num_elem);
for i=1:num_elem_t3
    elements(elem_t3(1,i)) = ...
        fem.analysis.plane.T3(nodes(elem_t3(5:end,i)),props(elem_t3(2,i),elem_t3(3,i)),constitutive);
end
for i=1:num_elem_t6
    elements(elem_t6(1,i)) = ...
        fem.analysis.plane.T6(nodes(elem_t6(5:end,i)),props(elem_t6(2,i),elem_t6(3,i)),constitutive);
end
for i=1:num_elem_q4
    elements(elem_q4(1,i)) = ...
        fem.analysis.plane.Q4(nodes(elem_q4(5:end,i)),props(elem_q4(2,i),elem_q4(3,i)),constitutive);
end
for i=1:num_elem_q8
    connect = elem_q8(5:end,i);
    order = [1,3,5,7,2,4,6,8];
    connect = connect(order);
    elements(elem_q8(1,i)) = ...
        fem.analysis.plane.Q8(nodes(connect),props(elem_q8(2,i),elem_q8(3,i)),constitutive);
end
load = zeros(num_nodes,4);
for i=1:num_nodal_load
    px = nodal_load(2,i);
    if px ~= 0
        load(nodal_load(1,i),1) = 1;
        load(nodal_load(1,i),3) = load(nodal_load(1,i),3) + px;
    end
    py = nodal_load(3,i);
    if py ~= 0
        load(nodal_load(1,i),2) = 1;
        load(nodal_load(1,i),4) = load(nodal_load(1,i),4) + py;
    end
end
for i = 1:num_elem_load
    el = elements(elem_load(1,i));
    for no = 1:length(el.Nodes)
        if el.Nodes(no).id == elem_load(2,i)
            break;
        end
    end
    x1 = el.Nodes(no).Coords(1);
    y1 = el.Nodes(no).Coords(2);
    x2 = el.Nodes(no+1).Coords(1);
    y2 = el.Nodes(no+1).Coords(2);
    dx = x2-x1;
    dy = y2-y1;
    L = sqrt(dx*dx+dy*dy);
    qx = elem_load(5,i);
    if qx ~= 0
        p = L/30*[4,2,-1;2,16,2;-1,2,4]*[qx;qx;qx];
        load(el.Nodes(no).id,1)=1;
        load(el.Nodes(no+1).id,1)=1;
        load(el.Nodes(no+4).id,1)=1;
        load(el.Nodes(no).id,3)=load(el.Nodes(no).id,3)+p(1);
        load(el.Nodes(no+1).id,3)=load(el.Nodes(no+1).id,3)+p(3);
        load(el.Nodes(no+4).id,3)=load(el.Nodes(no+4).id,3)+p(2);
    end
    qy = elem_load(6,i);
    if qy ~= 0
        p = L/30*[4,2,-1;2,16,2;-1,2,4]*[qy;qy;qy];
        load(el.Nodes(no).id,2)=1;
        load(el.Nodes(no+1).id,2)=1;
        load(el.Nodes(no+4).id,2)=1;
        load(el.Nodes(no).id,4)=load(el.Nodes(no).id,4)+p(1);
        load(el.Nodes(no+1).id,4)=load(el.Nodes(no+1).id,4)+p(3);
        load(el.Nodes(no+4).id,4)=load(el.Nodes(no+4).id,4)+p(2);
    end    
end
for i = 1:num_nodes
    dir = [0,0];
    count = 0;
    if load(i,1) == 1
        count = count + 1;
        dir(count) = 1;
    end
    if load(i,2) == 1
        count = count + 1;
        dir(count) = 2;
    end
    if count > 0
        dir = dir(1:count);
        nodes(i).setForces(dir,load(i,2+dir));
    end
end
end
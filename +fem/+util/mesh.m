function [coords,connect] = mesh(dimensions, subdiv, geometry, order)

lx = dimensions(1);
ly = dimensions(2);
nx = subdiv(1);
ny = subdiv(2);
dx = lx/nx; %Width of the element
dy = ly/ny; %Heigth of the element

num_nodes = (nx+1)*(ny+1);
num_edges = ny*(nx+1)+nx*(ny+1);
num_faces = nx*ny;
nodes_per_face = 4;
if strcmp(geometry,'triangle')
    num_edges = num_edges + num_faces;
    num_faces = num_faces * 2;
    nodes_per_face = 3;
end
if strcmp(order,'quad')
    num_nodes = num_nodes + num_edges;
    num_edges = num_edges * 2;
    nodes_per_face = nodes_per_face * 2;
end
assert(num_nodes+num_faces == num_edges+1, 'Does not satisfy Euler formula')
coords = zeros(num_nodes, 2);
n = 0;
for i=1:(nx+1)
    for j=1:(ny+1)
        n = n + 1;
        coords(n,1) = (i-1)*dx;
        coords(n,2) = (j-1)*dy;
    end
end
if strcmp(order,'quad')
    for i=1:(nx+1)
        for j=1:ny
            n = n + 1;
            coords(n,1) = (i-1)*dx;
            coords(n,2) = (j-1)*dy+dy/2;
        end
    end
    for i=1:nx
        for j=1:(ny+1)
            n = n + 1;
            coords(n,1) = (i-1)*dx+dx/2;
            coords(n,2) = (j-1)*dy;
        end
    end
    if strcmp(geometry,'triangle')
        for i=1:nx
            for j=1:ny
                n = n + 1;
                coords(n,1) = (i-1)*dx+dx/2;
                coords(n,2) = (j-1)*dy+dy/2;
            end
        end
    end
end

connect = zeros(num_faces, nodes_per_face);
e = 0;
for i=1:nx
    for j=1:ny
        e = e + 1;
        a = j + (ny+1)*(i-1);
        b = j + (ny+1)*i;
        c = (j+1) + (ny+1)*i;
        d = (j+1) + (ny+1)*(i-1);
        if strcmp(order,'quad')
            ab = a + (nx+1)*(ny+1)+ny*(nx+1);
            bc = b + (nx+1)*(ny+1) - i;
            cd = d + (nx+1)*(ny+1)+ny*(nx+1);
            da = a + (nx+1)*(ny+1) - (i-1);
            if strcmp(geometry,'triangle')
                center = a + (nx+1)*(ny+1)+ny*(nx+1)+nx*(ny+1) - (i-1);
            end
        end

        if strcmp(geometry,'triangle')
            connect(2*e-1, 1) = a;
            connect(2*e-1, 2) = b;
            connect(2*e-1, 3) = c;
            connect(2*e, 1) = c;
            connect(2*e, 2) = d;
            connect(2*e, 3) = a;
            if strcmp(order,'quad')
                connect(2*e-1, 4) = ab;
                connect(2*e-1, 5) = bc;
                connect(2*e-1, 6) = center;
                connect(2*e, 4) = cd;
                connect(2*e, 5) = da;
                connect(2*e, 6) = center;
            end
        else
            connect(e, 1) = a;
            connect(e, 2) = b;
            connect(e, 3) = c;
            connect(e, 4) = d;
            if strcmp(order,'quad')
                connect(e, 5) = ab;
                connect(e, 6) = bc;
                connect(e, 7) = cd;
                connect(e, 8) = da;
            end
        end
    end
end

end


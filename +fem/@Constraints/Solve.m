function Solve(fe)

% Dealing redundant constraints
MAT = fem.util.frref([fe.C(:,fe.dof),eye(fe.num_ctr)]);
num_var = sum(fe.dof);
tol = 1e-3*max(max(MAT(:, 1:num_var)));

num_rdt = 0; % number of redundant constraints
for i=1:fe.num_ctr
    row = fe.num_ctr - i + 1;
    zero_row = true;
    for j=1:num_var
        if abs(MAT(row,j)) > tol
            zero_row = false;
            break;
        end
    end
    if zero_row
        num_rdt = num_rdt + 1;
    else
        break;
    end
end

H = MAT(1 : fe.num_ctr - num_rdt, 1 : num_var);
Z = MAT(1 : fe.num_ctr - num_rdt, num_var+1 : num_var + fe.num_ctr);
q = fe.q-fe.C(:,fe.fix)*fe.d(fe.fix);
p = Z*q;

if num_rdt > 0
    warning('Redundant constrained structure');
    S = MAT(fe.num_ctr - num_rdt + 1 : fe.num_ctr, num_var+1 : num_var + fe.num_ctr);
    y = S*q;
    if ~isempty(find(y~=0, 1))
        error('Kinematically inadmissible structure');
    end
end

%--------------------------------------------------------------------------
% Solving
%--------------------------------------------------------------------------

% Compute unconstrained displacements and matrix A
MAT = fe.K(fe.dof,fe.dof)\[fe.f(fe.dof)-fe.K(fe.dof,fe.fix)*fe.d(fe.fix), H'];
u = MAT(:,1);
A = MAT(:,2 : fe.num_ctr - num_rdt + 1);

% Compute lagrange multipliers with respect to [H]{D} = {P}
M = H*A;
l = M\(H*u - p);

% Compute displacements
d = u - A*l;
fe.d(fe.dof) = d;

% Compute particular lagrange multipliers with respect to [C]{D} = {Q} 
fe.Lambda = Z'*l;

% Compute homogeneous lagrange multipliers with respect to [C]{D} = {Q} 
if num_rdt > 0
    d_u = zeros(size(fe.d));
    d_u(fe.dof) = d - u;

    % MAT = zeros(num_rdt);
    % VEC = zeros(num_rdt,1);
    % 
    % ctr_count = 0;
    % for el = fe.Elements
    %     if isa(el, 'fem.constraint.strain.ConstrainedElement')
    % 
    %         % Computing number of nodes per elements
    %         num_elm_node = length(el.Nodes);
    % 
    %         % Computing number of degree of freedom per element
    %         num_elm_dof = sum([el.Nodes.num_dof]);
    % 
    %         % Initializing gather vector
    %         g = zeros(1,num_elm_dof);
    % 
    %         % computing gather vector
    %         for i=1:num_elm_node
    %             num_node_dof = el.Nodes(i).num_dof;
    %             for j=1:num_node_dof
    %                 g(j+(i-1)*num_node_dof) = j+(el.Nodes(i).id-1)*num_node_dof;
    %             end
    %         end
    % 
    %         % computing element stiffness matrix
    %         K_e = el.Stiffness;
    % 
    %         % retrieving member displacements from global solution
    %         d_u_el = d_u(g);
    % 
    %         % computing element constraint matrix and the number of constraints
    %         mbr_num_ctr = el.NumberOfConstraints();
    %         C_e = el.ConstraintSystem();
    % 
    %         % computing stack vector
    %         s = (ctr_count + 1) : (ctr_count + mbr_num_ctr);
    % 
    %         % retrieving member Lagrange multipliers from global solution
    %         l = fe.Lambda(s);
    % 
    %         % Adding internal forces due to strain constraints
    %         f = K_e*d_u_el + C_e'*l;
    % 
    %         sig = S(:,s)';
    % 
    %         sig = C_e'*sig;
    % 
    %         VEC = VEC - sig'*f;
    % 
    %         MAT = MAT + sig'*sig;
    % 
    %         ctr_count = ctr_count + mbr_num_ctr;
    % 
    %     end    
    % 
    % end
    % 
    % % [a,~] = pcg(2*MAT,VEC);
    % a = 2*MAT\VEC;

    num_dof = 0;
    for el = fe.Elements
        if isa(el, 'fem.constraint.strain.ConstrainedElement')
            % Computing number of degree of freedom per element
            num_elm_dof = sum([el.Nodes.num_dof]);
            num_dof = num_dof + num_elm_dof;
        end
    end

    f_local_dofs = zeros(num_dof, 1);
    mat_local_dofs = zeros(num_dof, num_rdt);

    ctr_count = 0;
    dof_count = 0;
    for el = fe.Elements
        if isa(el, 'fem.constraint.strain.ConstrainedElement')

            % Computing number of nodes per elements
            num_elm_node = length(el.Nodes);

            % Computing number of degree of freedom per element
            num_elm_dof = sum([el.Nodes.num_dof]);

            % Initializing gather vector
            g = zeros(1,num_elm_dof);

            % computing gather vector
            for i=1:num_elm_node
                num_node_dof = el.Nodes(i).num_dof;
                for j=1:num_node_dof
                    g(j+(i-1)*num_node_dof) = j+(el.Nodes(i).id-1)*num_node_dof;
                end
            end

            % computing element stiffness matrix
            K_e = el.Stiffness;

            % retrieving member displacements from global solution
            d_u_el = d_u(g);

            % computing element constraint matrix and the number of constraints
            mbr_num_ctr = el.NumberOfConstraints();
            C_e = el.ConstraintSystem();

            % computing stack vector
            s = (ctr_count + 1) : (ctr_count + mbr_num_ctr);

            % retrieving member Lagrange multipliers from global solution
            l = fe.Lambda(s);

            % Adding internal forces due to strain constraints
            f = K_e*d_u_el + C_e'*l;

            f_local_dofs(dof_count+1:dof_count+num_elm_dof) = f;

            sig = S(:,s)';

            sig = C_e'*sig;

            mat_local_dofs(dof_count+1:dof_count+num_elm_dof,:) = sig;

            dof_count = dof_count + num_elm_dof;
            ctr_count = ctr_count + mbr_num_ctr;
        end
    end
    obj_func = zeros(1+num_rdt,1);
    obj_func(1) = 1;
    one_vec = ones(num_dof, 1);
    scl = 1/max(mat_local_dofs(:));
    A = [-one_vec, mat_local_dofs*scl; -one_vec, -mat_local_dofs*scl];
    b = [-f_local_dofs;f_local_dofs]*scl;
    ub = inf*ones(1+num_rdt,1);
    lb = -inf*ones(1+num_rdt,1);
    lb(1) = 0.;
    a = linprog(obj_func,A,b,[],[],lb,ub);
    a = a(2:end);

    
    % Compute lagrange multipliers with respect to [C]{D} = {Q}
    fe.Lambda = fe.Lambda + S'*a;

end
function Store(fem)
%Store Stores the lagrange multipliers at constrained elements
Store@fem.Analysis(fem);

% Constraint counter
ctr_count = 0;
for el = fem.Elements
    if isa(el, 'fem.constraint.strain.ConstrainedElement')
        
        % computing element number of constraints
        mbr_num_ctr = el.NumberOfConstraints();
        
        % computing stack vector
        s = (ctr_count + 1) : (ctr_count + mbr_num_ctr);
        
        % retrieving member Lagrange multipliers from global solution
       el.Lambda = fem.Lambda(s);
    end
end
end
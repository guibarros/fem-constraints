function Initialize(fem)
Initialize@fem.Analysis(fem);

% Computing number of constraints
fem.num_ctr = 0; %initialization

% Loop over elements
for el = fem.Elements
    if isa(el, 'fem.constraint.strain.ConstrainedElement')
        fem.num_ctr = fem.num_ctr + el.NumberOfConstraints();
    end
end

% initialization of constraint matrix
fem.C = zeros(fem.num_ctr, fem.num_dof);

% initialization of constraint vector
fem.q = zeros(fem.num_ctr, 1);

end
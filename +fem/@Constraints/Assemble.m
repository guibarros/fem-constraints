function Assemble(fem)
fem.ctr_count = 0;
Assemble@fem.Analysis(fem);
fem.C = sparse(fem.C);
end
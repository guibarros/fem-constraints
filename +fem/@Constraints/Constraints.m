classdef Constraints < fem.Analysis
    properties (SetAccess = private)
        num_ctr   double = double.empty
        ctr_count double = double.empty
        C         double = double.empty
        q         double = double.empty
        Lambda    double = double.empty
    end
    methods (Access = protected)
        Initialize(fem)
        Assemble(fem)
        AssembleElement(fem,elem,gather)
        Solve(fem)
        Store(fem)
    end
end
function AssembleElement(fem,el,g)

AssembleElement@fem.Analysis(fem,el,g);

% Element constraints
if isa(el, 'fem.constraint.strain.ConstrainedElement')
    
    % computing element constraint matrix and the number of constraints
    mbr_num_ctr = el.NumberOfConstraints();
    [C,q] = el.ConstraintSystem();
    
    % computing stack vector
    s = (fem.ctr_count + 1) : (fem.ctr_count + mbr_num_ctr);
    
    % Assembling the global constraint matrix
    fem.C(s,g) = C;
    fem.q(s) = q;
    fem.ctr_count = fem.ctr_count + mbr_num_ctr;
    
end

end
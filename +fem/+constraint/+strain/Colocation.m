classdef Colocation < fem.constraint.strain.ConstrainedElement
    properties (SetAccess = private)
        Points  double           = double.empty
    end
    methods
        function obj = Colocation(conc_elem, A, b, pts)
            obj@fem.constraint.strain.ConstrainedElement(conc_elem, A,b);
            obj.Points = pts;
        end
        function [C,q] = ConstraintSystem(obj)
            nP = size(obj.Points,1);
            num_cnstr_int_point = size(obj.A,1);
            num_cnstr = nP*num_cnstr_int_point;
            num_dof = sum([obj.Nodes.num_dof]);
            C=zeros(num_cnstr, num_dof);
            q=zeros(num_cnstr, 1);
            count = 0;
            for i=1:nP
                B = obj.BMatrix(obj.Points(i,:));
                C(count+1:count+num_cnstr_int_point,:) = obj.A*B;
                q(count+1:count+num_cnstr_int_point)   = obj.b;
                count = count + num_cnstr_int_point;
            end
        end
        function num = NumberOfConstraints(obj)
            nP = size(obj.Points,1);
            num_cnstr_int_point = size(obj.A,1);
            num = nP*num_cnstr_int_point;
        end
    end
end
classdef ConstrainedElement < fem.element.Base
    properties (SetAccess = private)
        Element fem.element.Base = fem.element.Base.empty
        A       double           = double.empty
        b       double           = double.empty
    end
    properties (SetAccess = ?fem.Constraints)
        Lambda double = double.empty
    end
    methods
        function elem = ConstrainedElement(conc_elem, A, b)
            elem@fem.element.Base(conc_elem.Nodes);
            elem.Element = conc_elem;
            elem.A = A;
            elem.b = b;
        end
        function k = Stiffness(elem)
            k = elem.Element.Stiffness();
        end
        function phi = ShapeFunc(elem, p)
            phi = elem.Element.ShapeFunc(p);
        end
        function [B,J] = BMatrix(elem, p)
            [B,J] = elem.Element.BMatrix(p);
        end
        function epsilon = CornerStrain(elem)
            epsilon = elem.Element.CornerStrain();
        end
        function sigma = CornerStress(elem)
            
            % Get constraint matrix
            C_e = elem.ConstraintSystem();
        
            % Internal forces due to strain constraints
            f = C_e'*elem.Lambda;

            % computing element stiffness matrix
            K_e = elem.Stiffness();

            % solve self balanced system of equations
            [u,~] = pcg(K_e,f);
            sigma1 = elem.Element.CornerStress();
            auxNodes = copy(elem.Nodes);
            node_count = 0;
            for no=auxNodes
                node_count = node_count + 1;
                no.setForces([],[]);
                no_dof = no.num_dof*(node_count-1)+1:no.num_dof*node_count;
                no.setDisplacements(1:no.num_dof,u(no_dof));                
            end
            elem.Element.Nodes = auxNodes;
            sigma2 = elem.Element.CornerStress();
            sigma = sigma1 + sigma2;
            elem.Element.Nodes = elem.Nodes;
        end
        function [x,y] = CornerPoints(elem)
            [x,y] = elem.Element.CornerPoints();
        end
        function int = Integration(elem)
            int = elem.Element.Integration();
        end
    end
    methods (Abstract)
        [C,q] = ConstraintSystem(obj);
        num = NumberOfConstraints(obj);
    end
end
classdef Integral < fem.constraint.strain.ConstrainedElement
    properties (SetAccess = private)
        Psi function_handle = function_handle.empty
    end
    methods
        function obj = Integral(conc_elem, A, b, Psi)
            obj@fem.constraint.strain.ConstrainedElement(conc_elem, A, b);
            obj.Psi = Psi;
        end
        function [C,q] = ConstraintSystem(obj)
            int = obj.Integration();
            C = int.eval(@(x,y)obj.ConstraintInt([x,y]));
            q = int.eval(@(x,y)obj.VecInt([x,y]));
        end
        function num = NumberOfConstraints(obj)
            num = size(obj.Psi([0,0]),2);
        end
        function sigma = CornerStress(obj)
            sigma_elastic = obj.Element.CornerStress();
            [x, y] = obj.CornerPoints();
            numCornerPoints = length(x);
            sigma_constraint = zeros(3,numCornerPoints);
            for i=1:numCornerPoints
                sigma_constraint(:,i) = obj.A'*obj.Psi([x(i),y(i)])*obj.Lambda;
            end
            % int = obj.Integration();
            % [x, y] = int.IntegrationPoints(int.NumPoints);
            % sigma_constraint = zeros(3,int.NumPoints);
            % for i=1:int.NumPoints
            %     sigma_constraint(:,i) = obj.A'*obj.Psi([x(i),y(i)])*obj.Lambda;
            % end
            % if size(sigma_constraint,2) == 1
            %     [x, ~] = obj.CornerPoints();
            %     sigma_constraint = repmat(sigma_constraint,1,length(x));
            % else
            %     sxx = fit([x',y'],sigma_constraint(1,:)','poly22');
            %     syy = fit([x',y'],sigma_constraint(2,:)','poly22');
            %     txy = fit([x',y'],sigma_constraint(3,:)','poly22');
            %     [x, y] = obj.CornerPoints();
            %     sigma_constraint = [sxx(x,y);syy(x,y);txy(x,y)];                
            % end
            sigma = sigma_elastic + sigma_constraint;
        end
    end
    methods (Access = private)
        function c = ConstraintInt(obj,p)
            [B,J] = obj.BMatrix(p);
            c = obj.Psi(p)'*obj.A*B*J;
        end
        function v = VecInt(obj,p)
            [~,J] = obj.BMatrix(p);
            v = obj.Psi(p)'*obj.b*J;
        end
    end
end
function Store(fem)
%Store Stores the calculated values of nodal forces and displcaments

% Loop over nodes and retrieves forces and displacements from global solution 
for i=1:fem.num_nds
    j=1:fem.Nodes(i).num_dof;
    fem.Nodes(i).Displacement = fem.d((i-1)*fem.Nodes(i).num_dof+j)';
    fem.Nodes(i).Force = fem.f((i-1)*fem.Nodes(i).num_dof+j)';
end
end
function Initialize(fem)
%Initialize Computes number of nodes, dofs and elements and allocate data
%   Computes number of nodes, number of elements and total number of 
%   degrees of freedom, both fixed and free. Pre-allocate the stiffness
%   matrix and the displacement and force vector.

% Computing number of nodes
fem.num_nds = length(fem.Nodes);

for i=1:fem.num_nds
    fem.Nodes(i).id = i;
end

% Computing number of elements
fem.num_elm = length(fem.Elements);

% Computing number of degree of freedom
fem.num_dof = sum([fem.Nodes.num_dof]);

% initialization of stiffness matrix
fem.K = zeros(fem.num_dof,fem.num_dof);

% initialization of global displacement vector
fem.d = zeros(fem.num_dof,1);

% initialization of global force vector
fem.f = zeros(fem.num_dof,1);

% Note that these matrices have size of all degrees of freedom, and after
% their assemblage, the treatment of boundary condition must be carried out
end
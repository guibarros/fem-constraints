function Assemble(fem)
%Assemble Assembles system of equations
%   Loops over elements to compute stiffness matrix. For each element,
%   computes gather vector and add element stiffness into global system of
%   equations.


% Loop over the members to assemble the global stiffness matrix and the
% global constraint matrix. 
for el = fem.Elements

    % Computing number of nodes per elements
    num_elm_node = length(el.Nodes);

    % Computing number of degree of freedom per element
    num_elm_dof = sum([el.Nodes.num_dof]);

    % Initializing gather vector
    g = zeros(1,num_elm_dof);
    
    % computing gather vector
    for i=1:num_elm_node
        num_node_dof = el.Nodes(i).num_dof;
        for j=1:num_node_dof
            g(j+(i-1)*num_node_dof) = j+(el.Nodes(i).id-1)*num_node_dof;
        end
    end
    
    fem.AssembleElement(el,g);
    
end
fem.K = sparse(fem.K);
end
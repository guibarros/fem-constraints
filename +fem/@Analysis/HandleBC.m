function HandleBC(fem)
%HandleBC Handles boundary conditions
%   Computes global force vector. Adds prescribed displacements to
%   displacement vector. Removes undetermined degrees of freedom from
%   global system of equations.

% Assembling the global load vector
for i=1:fem.num_nds
    if ~isempty(fem.Nodes(i).BC)
        loc = fem.Nodes(i).BC.Force;
        f_pres = fem.Nodes(i).Force(loc);
        fem.f((i-1)*fem.Nodes(i).num_dof+loc) = f_pres;
    end
end

% Computing vector of fixed and free dof's. It shall be used to handle
% boundary conditions of displacements.
fem.dof = true(1,fem.num_dof);
for i=1:fem.num_nds
    if ~isempty(fem.Nodes(i).BC)
        loc = fem.Nodes(i).BC.Displacement;
        d_pres = fem.Nodes(i).Displacement(loc);
        fem.dof((i-1)*fem.Nodes(i).num_dof+loc) = false;
        fem.d((i-1)*fem.Nodes(i).num_dof+loc) = d_pres;
    end
end

% Dealing undetermined dof's
for i=1:fem.num_dof
    if fem.K(i,i) == 0
        fem.dof(i) = false;
        fem.d(i) = 0;
    end
end

% Creating vector of fixed displacements
fem.fix = ~fem.dof;

end
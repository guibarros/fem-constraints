function AssembleElement(fem,el,g)

% computing element stiffness matrix
K_e = el.Stiffness;

% Assembling the global stiffness matrix
fem.K(g,g) = fem.K(g,g) + K_e;

end
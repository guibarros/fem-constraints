function Solve(fem)
%Solve Find the solution of the system of equations
%   First, the displacements are calculated. Then the reaction forces at
%   supported directions are computed.

% Calculate displacements at dof's
fem.d(fem.dof) = fem.K(fem.dof,fem.dof)\(fem.f(fem.dof)-fem.K(fem.dof,fem.fix)*fem.d(fem.fix));
% Calculate support reactions
fem.f(fem.fix) = fem.K(fem.fix,fem.dof)*fem.d(fem.dof) + fem.K(fem.fix,fem.fix)*fem.d(fem.fix);
end
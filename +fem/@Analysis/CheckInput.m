function CheckInput(fem)
if isempty(fem.Nodes)
    error('Nodes must be defined before running analysis');
end
if isempty(fem.Elements)
    error('Elements must be defined before running analysis');
end
end
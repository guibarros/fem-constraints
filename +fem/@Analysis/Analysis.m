classdef Analysis < handle
    properties
        Nodes fem.node.Base = fem.node.Base.empty
        Elements fem.element.Base = fem.element.Base.empty
    end
    properties (SetAccess = private)
        num_nds double = double.empty
        num_elm double = double.empty
        num_dof double = double.empty
        K       double = double.empty
        dof
        fix
    end
    properties (SetAccess = protected)
        d       double = double.empty
        f       double = double.empty
    end
    properties (Hidden, Access = private)
        Complete = false
    end
    methods
        function set.Nodes(fem, nodes)
            fem.Nodes = nodes;
            fem.stateChange();
        end
        function set.Elements(fem, elements)
            fem.Elements = elements;
            fem.stateChange();
        end
        function set.Complete(fem, state)
            fem.Complete = logical(state);
        end
        function stateChange(fem)
            fem.Complete = false;
        end
        Run(fem);
    end
    methods (Access = protected)
        CheckInput(fem)
        Initialize(fem)
        Assemble(fem)
        AssembleElement(fem,elem,gather)
        HandleBC(fem)
        Solve(fem)
        Store(fem)
    end
end
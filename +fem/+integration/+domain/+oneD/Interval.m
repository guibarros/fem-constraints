classdef Interval < fem.integration.domain.oneD.Base
    properties
        a (1,1) double
        b (1,1) double
    end
    methods
        function inter = Interval(a,b)
            inter.a = a;
            inter.b = b;
        end
    end
end
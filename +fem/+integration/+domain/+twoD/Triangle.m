classdef Triangle < fem.integration.domain.twoD.Base
    properties
        p1 double
        p2 double
        p3 double
    end
    methods
        function tri = Triangle(p1, p2, p3)
            if length(p1) ~= 2 || length(p2) ~= 2 || length(p3) ~= 2
                error('Two dimensional triangle.')
            end
            tri.p1 = p1;
            tri.p2 = p2;
            tri.p3 = p3;
        end
    end
end
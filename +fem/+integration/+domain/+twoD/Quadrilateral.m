classdef Quadrilateral < fem.integration.domain.twoD.Base
    properties
        p1 double
        p2 double
        p3 double
        p4 double
    end
    methods
        function quad = Quadrilateral(p1, p2, p3, p4)
            if length(p1) ~= 2 || length(p2) ~= 2 || length(p3) ~= 2 || length(p4) ~= 2
                error('Two dimensional quadrilateral.')
            end
            quad.p1 = p1;
            quad.p2 = p2;
            quad.p3 = p3;
            quad.p4 = p4;
        end
    end
end
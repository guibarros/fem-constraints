classdef Base < fem.integration.Base
    methods
        function result = eval(obj,fun,num)
            if nargin == 2
                num = obj.NumPoints;
            end
            [x,y,weigths] = obj.IntegrationPoints(num);
            result = 0;
            for i = 1:num
                result = result + fun(x(i),y(i))*weigths(i);
            end
        end
    end
end
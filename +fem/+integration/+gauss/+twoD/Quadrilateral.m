classdef Quadrilateral < fem.integration.domain.twoD.Quadrilateral
    methods
        function quad = Quadrilateral(p1, p2, p3, p4)
            quad@fem.integration.domain.twoD.Quadrilateral(p1, p2, p3, p4);
        end
        function [x,y,w] = IntegrationPoints(obj, num)
            t = zeros(1,sqrt(num));
            w = zeros(1,sqrt(num));
            switch sqrt(num)
                case 1
                    % coords:
                    t(1) = 0.0;
                    % weights:
                    w(1) = 2.0;
                case 2
                    % coords:
                    t(1) = -1/sqrt(3);
                    t(2) = -t(1);
                    % weights:
                    w(1) = 1.0;
                    w(2) = w(1);
                case 3
                    % coords:
                    t(1) = -sqrt(3/5);
                    t(2) = 0.0;
                    t(3) = -t(1);
                    % weights:
                    w(1) = 5/9;
                    w(2) = 8/9;
                    w(3) = w(1);
                case 4
                    % coords:
                    t(1) = -sqrt(3/7+2/7*sqrt(6/5));
                    t(2) = -sqrt(3/7-2/7*sqrt(6/5));
                    t(3) = -t(2);
                    t(4) = -t(1);
                    % weights:
                    w(1) = (18-sqrt(30))/36;
                    w(2) = (18+sqrt(30))/36;
                    w(3) = w(2);
                    w(4) = w(1);
                case 5
                    t(1) = -sqrt(5+2*sqrt(10/7))/3;
                    t(2) = -sqrt(5-2*sqrt(10/7))/3;
                    t(3) = .0;
                    t(4) = -t(2);
                    t(5) = -t(1);
                    w(1) = (322-13*sqrt(70))/900;
                    w(2) = (322+13*sqrt(70))/900;
                    w(3) = 128/225;
                    w(4) = w(2);
                    w(5) = w(1);
                otherwise
                    disp('Gauss order not available.')
            end
            x = repmat(t ,sqrt(num),1); x=x(:)';
            y = repmat(t',1,sqrt(num)); y=y(:)';
            wi = repmat(w ,sqrt(num),1); wi=wi(:);
            wj = repmat(w',1,sqrt(num)); wj=wj(:);
            
            N1 = (1-x).*(1-y)/4;
            N2 = (1+x).*(1-y)/4;
            N3 = (1+x).*(1+y)/4;
            N4 = (1-x).*(1+y)/4;
            
            X = [obj.p1(1),obj.p1(2);
                obj.p2(1),obj.p2(2);
                obj.p3(1),obj.p3(2);
                obj.p4(1),obj.p4(2)];
            
            dN1dx=-(1-y)/4;
            dN2dx= (1-y)/4;
            dN3dx= (1+y)/4;
            dN4dx=-(1+y)/4;
            
            dN1dy=-(1-x)/4;
            dN2dy=-(1+x)/4;
            dN3dy= (1+x)/4;
            dN4dy= (1-x)/4;
            
            w = zeros(1,num);
            for i=1:num
                dN=[dN1dx(i) dN2dx(i) dN3dx(i) dN4dx(i);
                    dN1dy(i) dN2dy(i) dN3dy(i) dN4dy(i)];
                J=dN*X;
                detJ = J(1,1)*J(2,2) - J(1,2)*J(2,1);
                w(i) = wi(i)*wj(i)*detJ;
            end
            x = N1*obj.p1(1) + N2*obj.p2(1) + N3*obj.p3(1) + N4*obj.p4(1);
            y = N1*obj.p1(2) + N2*obj.p2(2) + N3*obj.p3(2) + N4*obj.p4(2);
        end
    end
end
classdef Triangle < fem.integration.domain.twoD.Triangle
    methods
        function tri = Triangle(p1, p2, p3)
            tri@fem.integration.domain.twoD.Triangle(p1, p2, p3);
        end
        function [x,y,w] = IntegrationPoints(obj, num)
            x = zeros(1,num);
            y = zeros(1,num);
            w = zeros(1,num);
            switch num
                case 1
                    % coords:
                    x(1) = 1/3;
                    y(1) = x(1);
                    % weights:
                    w(1) = 0.5;
                case 3
                    % coords:
                    x(1) = 1/6;
                    y(1) = x(1);
                    x(2) = 2/3;
                    y(2) = y(1);
                    x(3) = x(1);
                    y(3) = x(2);
                    % weights:
                    w(1) = 1/6;
                    w(2) = w(1);
                    w(3) = w(1);
                case 4
                    % coords:
                    x(1) = 1/3;
                    y(1) = x(1);
                    x(2) = .2;
                    y(2) = x(2);
                    x(3) = .6;
                    y(3) = y(2);
                    x(4) = y(3);
                    y(4) = x(3);
                    % weights:
                    w(1) = -0.5625 * 0.5;
                    w(2) = 0.520833333333333 * 0.5;
                    w(3) = w(2);
                    w(4) = w(2);
                case 6
                    x(1) = 0.445948490915965;
                    y(1) = 0.445948490915965;
                    x(2) = 0.091576213509771;
                    y(2) = 0.09157621350977;
                    x(3) = 0.10810301816807;
                    y(3) = 0.445948490915965;
                    x(4) = 0.445948490915965;
                    y(4) = 0.10810301816807;
                    x(5) = 0.816847572980459;
                    y(5) = 0.09157621350977;
                    x(6) = 0.091576213509771;
                    y(6) = 0.816847572980458;
                    w(1) = 0.223381589678011 * 0.5;
                    w(2) = 0.109951743655322 * 0.5;
                    w(3) = w(1);
                    w(4) = w(1);
                    w(5) = w(2);
                    w(6) = w(2);
                otherwise
                    disp('Gauss order not available.')
            end
            N1 = 1-x-y;
            N2 = x;
            N3 = y;
            
            x = N1*obj.p1(1) + N2*obj.p2(1) + N3*obj.p3(1);
            y = N1*obj.p1(2) + N2*obj.p2(2) + N3*obj.p3(2);
            
            X = [obj.p1(1),obj.p1(2);
                obj.p2(1),obj.p2(2);
                obj.p3(1),obj.p3(2)];
            
            dN1dx=-1;
            dN2dx= 1;
            dN3dx= 0;
            
            dN1dy=-1;
            dN2dy=0;
            dN3dy=1;
            
            dN=[dN1dx dN2dx dN3dx;
                dN1dy dN2dy dN3dy];
            
            J=dN*X;
            J = J(1,1)*J(2,2) - J(1,2)*J(2,1);
            w = w*J;
        end
    end
end
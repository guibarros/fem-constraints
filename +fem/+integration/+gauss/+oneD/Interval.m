classdef Interval < fem.integration.domain.oneD.Interval
    methods
        function obj = Interval(a,b)
            obj@fem.integration.domain.oneD.Interval(a,b);
        end
        function [x,w] = IntegrationPoints(obj, num)
            x = zeros(1,num);
            w = zeros(1,num);
            switch num
                case 1
                    % coords:
                    x(1) = 0.0;
                    % weights:
                    w(1) = 2.0;
                case 2
                    % coords:
                    x(1) = -1/sqrt(3);
                    x(2) = -x(1);
                    % weights:
                    w(1) = 1.0;
                    w(2) = w(1);
                case 3
                    % coords:
                    x(1) = -sqrt(3/5);
                    x(2) = 0.0;
                    x(3) = -x(1);
                    % weights:
                    w(1) = 5/9;
                    w(2) = 8/9;
                    w(3) = w(1);
                case 4
                    % coords:
                    x(1) = -sqrt(3/7+2/7*sqrt(6/5));
                    x(2) = -sqrt(3/7-2/7*sqrt(6/5));
                    x(3) = -x(2);
                    x(4) = -x(1);
                    % weights:
                    w(1) = (18-sqrt(30))/36;
                    w(2) = (18+sqrt(30))/36;
                    w(3) = w(2);
                    w(4) = w(1);
                case 5
                    x(1) = -sqrt(5+2*sqrt(10/7))/3;
                    x(2) = -sqrt(5-2*sqrt(10/7))/3;
                    x(3) = .0;
                    x(4) = -x(2);
                    x(5) = -x(1);
                    w(1) = (322-13*sqrt(70))/900;
                    w(2) = (322+13*sqrt(70))/900;
                    w(3) = 128/225;
                    w(4) = w(2);
                    w(5) = w(1);
                otherwise
                    disp('Gauss order not available.')
            end
            x = (obj.b+obj.a)/2 + (obj.b-obj.a)/2*x;
            w = (obj.b-obj.a)/2*w;
        end
    end
end
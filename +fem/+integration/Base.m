classdef Base
    methods(Abstract)
        result = eval(obj,fun,num)
        varargout = IntegrationPoints(obj, num)
    end
    properties
        NumPoints = 0;
    end
end
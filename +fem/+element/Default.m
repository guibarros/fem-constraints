classdef Default < fem.element.Base
    methods
        function elem = Default()
            elem@fem.element.Base(fem.node.Base.empty);
        end
    end
    methods
        function k = Stiffness(~)
            k = [];
        end
        function phi = ShapeFunc(~, ~)
            phi = [];
        end
        function [B,J] = BMatrix(~, ~)
            B = [];
            J = [];
        end
        function epsilon = CornerStrain(~)
            epsilon = [];
        end
        function sigma = CornerStress(~)
            sigma = [];
        end
        function [x,y] = CornerPoints(~)
            x = [];
            y = [];
        end
        function int = Integration(~)
            int = [];
        end
    end
end
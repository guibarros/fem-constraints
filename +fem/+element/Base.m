classdef Base < handle & matlab.mixin.Heterogeneous
    properties (SetAccess = protected)
        Nodes fem.node.Base
    end
    methods
        function elem = Base(nodes)
            elem.Nodes = nodes;
        end
    end
    methods (Abstract)
        k       = Stiffness   (elem)
        phi     = ShapeFunc   (elem, p)
        [B,J]   = BMatrix     (elem, p)
        epsilon = CornerStrain(elem)
        sigma   = CornerStress(elem)
        [x,y]   = CornerPoints(elem)
        int     = Integration (elem)
    end
    methods (Static, Sealed, Access = protected)
        function defaultObject = getDefaultScalarElement
            defaultObject = fem.element.Default;
        end
    end
end
classdef Base < handle & matlab.mixin.Heterogeneous
    methods (Static, Sealed, Access = protected)
        function defaultObject = getDefaultScalarElement
            defaultObject = fem.element.property.Base;
        end
    end
end
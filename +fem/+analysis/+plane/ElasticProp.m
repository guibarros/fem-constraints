classdef ElasticProp < fem.element.property.Base
    properties %(SetAccess = private)
        E(1,1) double
        v(1,1) double
        t(1,1) double
    end
    methods
        function prop = ElasticProp(E, v, t)
            prop.E = E;
            prop.v = v;
            prop.t = t;
        end
    end
end
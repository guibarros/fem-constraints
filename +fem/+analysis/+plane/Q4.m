classdef Q4 < fem.analysis.plane.Continuum2D
    methods
        function elem = Q4(nodes, prop, constitutive)
            elem@fem.analysis.plane.Continuum2D(nodes, prop, constitutive);
        end
        function [k] = Stiffness(elem)
            int = elem.Integration();
            k = int.eval(@(x,y)elem.StiffnessInt([x,y]));
        end
        function k = StiffnessInt(elem,p)
            [B,J] = elem.BMatrix(p);
            C = elem.Constitutive();
            k = B'*C*B*J;
        end
        function epsilon = CornerStrain(elem)
            int = elem.Integration();
            [x, y] = int.IntegrationPoints(int.NumPoints);
            epsilon = zeros(3,4);
            for i=1:4
                B = elem.BMatrix([x(i),y(i)]);
                epsilon(:,i) = B*[elem.Nodes.Displacement]';
            end
            exx = fit([x',y'],epsilon(1,:)','poly11');
            eyy = fit([x',y'],epsilon(2,:)','poly11');
            gxy = fit([x',y'],epsilon(3,:)','poly11');
            [x,y] = elem.CornerPoints();
            epsilon = [exx(x,y);eyy(x,y);gxy(x,y)];
            % extra = zeros(4,4);
            % for i=1:4
            %     csi = x(i)*3;
            %     eta = y(i)*3;
            %     extra(i,1)=(1-csi)*(1-eta)/4;
            %     extra(i,2)=(1+csi)*(1-eta)/4;
            %     extra(i,3)=(1+csi)*(1+eta)/4;
            %     extra(i,4)=(1-csi)*(1+eta)/4;
            % end
            % epsilon = epsilon*extra';
        end
        function [x,y] = CornerPoints(elem)
            x = [-1,1,1,-1];
            y = [-1,-1,1,1];
        end
        function phi = ShapeFunc(elem, p)
            csi = p(1);
            eta = p(2);            
            phi_1=(1-csi)*(1-eta)/4;
            phi_2=(1+csi)*(1-eta)/4;
            phi_3=(1+csi)*(1+eta)/4;
            phi_4=(1-csi)*(1+eta)/4;            
            phi = [phi_1,phi_2,phi_3,phi_4];
        end
        function [B,J] = BMatrix(elem, p)
            csi = p(1);
            eta = p(2);
            X = zeros(4,2);
            for i=1:4
                X(i,1) = elem.Nodes(i).Coords(1);
                X(i,2) = elem.Nodes(i).Coords(2);
            end
            
            dN1dc=-(1-eta)/4;
            dN2dc=(1-eta)/4;
            dN3dc=(1+eta)/4;
            dN4dc=-(1+eta)/4;
            
            dN1de=-(1-csi)/4;
            dN2de=-(1+csi)/4;
            dN3de=(1+csi)/4;
            dN4de=(1-csi)/4;
            
            dNde=[dN1dc dN2dc dN3dc dN4dc;
                dN1de dN2de dN3de dN4de];
            
            J=dNde*X;
            
            dNdx=J\dNde;

            B = zeros(3, 8);
            for N=1:4
                for pdir=1:2
                    B(pdir, 2*N-2+pdir) = dNdx(pdir, N);
                    B(3, 2*N+1-pdir) = dNdx(pdir, N);
                end
            end
            
            J = det(J)*elem.Prop.t;
        end
        function [x,y] = coordinates(elem, p)
            x1 = elem.Nodes(1).Coords(1);
            y1 = elem.Nodes(1).Coords(2);
            x2 = elem.Nodes(2).Coords(1);
            y2 = elem.Nodes(2).Coords(2);
            x3 = elem.Nodes(3).Coords(1);
            y3 = elem.Nodes(3).Coords(2);
            x4 = elem.Nodes(4).Coords(1);
            y4 = elem.Nodes(4).Coords(2);
            
            X = [x1,y1;
                 x2,y2;
                 x3,y3;
                 x4,y4];
            
            N = elem.ShapeFunc(p);
            
            X = N*X;
            
            x = X(1);
            y = X(2);
            
        end
        function int = Integration(~)
            int = fem.integration.gauss.twoD.Quadrilateral(...
                [-1,-1],...
                [ 1,-1],...
                [ 1, 1],...
                [-1, 1]);
            int.NumPoints = 4;
        end
    end
end
classdef Pos2D < handle
    properties (SetAccess = private)
        Coords
        x_fix
        y_fix
        x_force_x
        y_force_x
        d_force_x
        x_force_y
        y_force_y
        d_force_y
        arrowSize
        Disp
        Connect
        Sxx
        Syy
        Txy
        Exx
        Eyy
        Gxy
        Connect_u
        Sxx_u
        Syy_u
        Txy_u
        Exx_u
        Eyy_u
        Gxy_u
        Connect_c
        Sxx_c
        Syy_c
        Txy_c
        Exx_c
        Eyy_c
        Gxy_c
    end
    
    properties (Access = public)
        PlotConstrained = true
        PlotUnconstrained = true
        PlotType = 'mesh'
        PlotAxis = ''
        Scale = 1.0
        maxValue = Inf
        minValue = -Inf
    end
    
    properties (Access = private)
        MaxScale = 0.0
    end
    methods
        function pos = Pos2D(nodes,elements)
            pos.Coords = reshape([nodes.Coords],2,length(nodes))';
            x_fix = [];
            y_fix = [];
            x_force_x = [];
            y_force_x = [];
            d_force_x = [];
            x_force_y = [];
            y_force_y = [];
            d_force_y = [];
            for n = 1:length(nodes)
                if ~isempty(nodes(n).BC)
                    if ~isempty(nodes(n).BC.Displacement)
                        x_fix = [x_fix; nodes(n).Coords(1)]; %#ok<AGROW>
                        y_fix = [y_fix; nodes(n).Coords(2)]; %#ok<AGROW>
                    end
                    if ~isempty(nodes(n).BC.Force)
                        for dir_id=1:length(nodes(n).BC.Force)
                            dir = nodes(n).BC.Force(dir_id);
                            if dir == 1
                                x_force_x = [x_force_x; nodes(n).Coords(1)]; %#ok<AGROW>
                                y_force_x = [y_force_x; nodes(n).Coords(2)]; %#ok<AGROW>
                                if nodes(n).Force(dir) > 0
                                    d_force_x = [d_force_x; 1]; %#ok<AGROW>
                                else
                                    d_force_x = [d_force_x; -1]; %#ok<AGROW>
                                end
                            elseif dir == 2
                                x_force_y = [x_force_y; nodes(n).Coords(1)]; %#ok<AGROW>
                                y_force_y = [y_force_y; nodes(n).Coords(2)]; %#ok<AGROW>
                                if nodes(n).Force(dir) > 0
                                    d_force_y = [d_force_y; 1]; %#ok<AGROW>
                                else
                                    d_force_y = [d_force_y; -1]; %#ok<AGROW>
                                end
                            end
                        end
                    end
                end
            end
            pos.x_fix = x_fix;
            pos.y_fix = y_fix;
            pos.x_force_x = x_force_x;
            pos.y_force_x = y_force_x;
            pos.d_force_x = d_force_x;
            pos.x_force_y = x_force_y;
            pos.y_force_y = y_force_y;
            pos.d_force_y = d_force_y;
            min_x = min(pos.Coords(:,1));
            min_y = min(pos.Coords(:,2));
            max_x = max(pos.Coords(:,1));
            max_y = max(pos.Coords(:,2));
            modelSize = max([max_x-min_x,max_y-min_y]);
            pos.arrowSize = modelSize/10;
            pos.Disp   = reshape([nodes.Displacement],2,length(nodes))';
            pos.CalculateScale();
            nodeValency = zeros(1,length(nodes));
            nodeValency_u = zeros(1,length(nodes));
            nodeValency_c = zeros(1,length(nodes));
            pos.Sxx = zeros(1,length(nodes));
            pos.Syy = zeros(1,length(nodes));
            pos.Txy = zeros(1,length(nodes));
            pos.Exx = zeros(1,length(nodes));
            pos.Eyy = zeros(1,length(nodes));
            pos.Gxy = zeros(1,length(nodes));
            pos.Sxx_u = zeros(1,length(nodes));
            pos.Syy_u = zeros(1,length(nodes));
            pos.Txy_u = zeros(1,length(nodes));
            pos.Exx_u = zeros(1,length(nodes));
            pos.Eyy_u = zeros(1,length(nodes));
            pos.Gxy_u = zeros(1,length(nodes));
            pos.Sxx_c = zeros(1,length(nodes));
            pos.Syy_c = zeros(1,length(nodes));
            pos.Txy_c = zeros(1,length(nodes));
            pos.Exx_c = zeros(1,length(nodes));
            pos.Eyy_c = zeros(1,length(nodes));
            pos.Gxy_c = zeros(1,length(nodes));
            elem_type_count = 0;
            for elem = elements
                c = [elem.Nodes.id];
                num_nodes = length(c);
                if num_nodes == 6
                    order = [1,4,2,5,3,6];
                elseif num_nodes == 8
                    order = [1,5,2,6,3,7,4,8];
                else
                    order = 1:num_nodes;
                end
                c = c(order);
                found = false;
                for i = 1:elem_type_count
                    if pos.Connect(i).NumNodes == num_nodes
                        found = true;
                        break;
                    end
                end
                if ~found
                    elem_type_count = elem_type_count + 1;
                    elem_type = elem_type_count;
                    pos.Connect(elem_type).NumNodes = num_nodes;
                    pos.Connect(elem_type).Ids = [];
                    pos.Connect_u(elem_type).NumNodes = num_nodes;
                    pos.Connect_u(elem_type).Ids = [];
                    pos.Connect_c(elem_type).NumNodes = num_nodes;
                    pos.Connect_c(elem_type).Ids = [];
                else
                    elem_type = i;
                end
                pos.Connect(elem_type).Ids = [pos.Connect(elem_type).Ids;c];
                s = elem.CornerStress();
                e = elem.CornerStrain();
                for i = 1:num_nodes
                    pos.Sxx(c(i)) = pos.Sxx(c(i)) + s(1,order(i));
                    pos.Syy(c(i)) = pos.Syy(c(i)) + s(2,order(i));
                    pos.Txy(c(i)) = pos.Txy(c(i)) + s(3,order(i));
                    pos.Exx(c(i)) = pos.Exx(c(i)) + e(1,order(i));
                    pos.Eyy(c(i)) = pos.Eyy(c(i)) + e(2,order(i));
                    pos.Gxy(c(i)) = pos.Gxy(c(i)) + e(3,order(i));
                    nodeValency(c(i)) = nodeValency(c(i)) + 1;
                end
                if isa(elem,'fem.constraint.strain.ConstrainedElement')
                    pos.Connect_c(elem_type).Ids = [pos.Connect_c(elem_type).Ids;c];
                    for i = 1:num_nodes
                        pos.Sxx_c(c(i)) = pos.Sxx_c(c(i)) + s(1,order(i));
                        pos.Syy_c(c(i)) = pos.Syy_c(c(i)) + s(2,order(i));
                        pos.Txy_c(c(i)) = pos.Txy_c(c(i)) + s(3,order(i));
                        pos.Exx_c(c(i)) = pos.Exx_c(c(i)) + e(1,order(i));
                        pos.Eyy_c(c(i)) = pos.Eyy_c(c(i)) + e(2,order(i));
                        pos.Gxy_c(c(i)) = pos.Gxy_c(c(i)) + e(3,order(i));
                        nodeValency_c(c(i)) = nodeValency_c(c(i)) + 1;
                    end
                else
                    pos.Connect_u(elem_type).Ids = [pos.Connect_u(elem_type).Ids;c];
                    for i = 1:num_nodes
                        pos.Sxx_u(c(i)) = pos.Sxx_u(c(i)) + s(1,order(i));
                        pos.Syy_u(c(i)) = pos.Syy_u(c(i)) + s(2,order(i));
                        pos.Txy_u(c(i)) = pos.Txy_u(c(i)) + s(3,order(i));
                        pos.Exx_u(c(i)) = pos.Exx_u(c(i)) + e(1,order(i));
                        pos.Eyy_u(c(i)) = pos.Eyy_u(c(i)) + e(2,order(i));
                        pos.Gxy_u(c(i)) = pos.Gxy_u(c(i)) + e(3,order(i));
                        nodeValency_u(c(i)) = nodeValency_u(c(i)) + 1;
                    end
                end
            end
            pos.Sxx = pos.Sxx./nodeValency;
            pos.Syy = pos.Syy./nodeValency;
            pos.Txy = pos.Txy./nodeValency;
            pos.Exx = pos.Exx./nodeValency;
            pos.Eyy = pos.Eyy./nodeValency;
            pos.Gxy = pos.Gxy./nodeValency;
            nodeValency_u(nodeValency_u==0) = 10000;
            pos.Sxx_u = pos.Sxx_u./nodeValency_u;
            pos.Syy_u = pos.Syy_u./nodeValency_u;
            pos.Txy_u = pos.Txy_u./nodeValency_u;
            pos.Exx_u = pos.Exx_u./nodeValency_u;
            pos.Eyy_u = pos.Eyy_u./nodeValency_u;
            pos.Gxy_u = pos.Gxy_u./nodeValency_u;
            nodeValency_c(nodeValency_c==0) = 10000;
            pos.Sxx_c = pos.Sxx_c./nodeValency_c;
            pos.Syy_c = pos.Syy_c./nodeValency_c;
            pos.Txy_c = pos.Txy_c./nodeValency_c;
            pos.Exx_c = pos.Exx_c./nodeValency_c;
            pos.Eyy_c = pos.Eyy_c./nodeValency_c;
            pos.Gxy_c = pos.Gxy_c./nodeValency_c;
        end
        function plot(pos,varargin)
            if pos.PlotConstrained == true && pos.PlotUnconstrained == true
                for connect = pos.Connect
                    
                    if strcmp(pos.PlotType,'mesh')
                        patch('Faces',connect.Ids,...
                            'Vertices',pos.Coords+pos.Disp*pos.MaxScale*pos.Scale,...
                            varargin{:});
                        hold on
                        scatter(pos.x_fix, pos.y_fix, 'x', 'LineWidth', 2);
                        for i = 1:length(pos.x_force_x)
                            if pos.d_force_x(i) == 1
                                quiver(pos.x_force_x(i), pos.y_force_x(i), pos.arrowSize, 0, 'r', 'LineWidth', 2);
                            elseif pos.d_force_x(i) == -1
                                quiver(pos.x_force_x(i)+pos.arrowSize, pos.y_force_x(i), -pos.arrowSize, 0, 'r', 'LineWidth', 2);
                            end
                        end
                        for i = 1:length(pos.x_force_y)
                            if pos.d_force_y(i) == 1
                                quiver(pos.x_force_y(i), pos.y_force_y(i), 0, pos.arrowSize, 'r', 'LineWidth', 2);
                            elseif pos.d_force_y(i) == -1
                                quiver(pos.x_force_y(i), pos.y_force_y(i)+pos.arrowSize, 0, -pos.arrowSize, 'r', 'LineWidth', 2);
                            end
                        end
                        hold off
                        title(pos.PlotType,'interpreter','latex');
                    else
                        if strcmp(pos.PlotType,'strain')
                            title(['$$\epsilon_{',pos.PlotAxis,'}$$'],'interpreter','latex');
                            if strcmp(pos.PlotAxis,'xx')
                                colorValues = pos.Exx;
                            elseif strcmp(pos.PlotAxis,'yy')
                                colorValues = pos.Eyy;
                            elseif strcmp(pos.PlotAxis,'xy')
                                title(['$$\gamma_{',pos.PlotAxis,'}$$'],'interpreter','latex');
                                colorValues = pos.Gxy;
                            end
                        elseif strcmp(pos.PlotType,'stress')
                            title(['$$\sigma_{',pos.PlotAxis,'}$$'],'interpreter','latex');
                            if strcmp(pos.PlotAxis,'xx')
                                colorValues = pos.Sxx;
                            elseif strcmp(pos.PlotAxis,'yy')
                                colorValues = pos.Syy;
                            elseif strcmp(pos.PlotAxis,'xy')
                                title(['$$\tau_{',pos.PlotAxis,'}$$'],'interpreter','latex');
                                colorValues = pos.Txy;
                            end
                        end
                        clampedValues = max(min(colorValues', pos.maxValue), pos.minValue);
                        patch('Faces',connect.Ids,...
                              'Vertices',pos.Coords+pos.Disp*pos.MaxScale*pos.Scale,...
                              'FaceVertexCData',clampedValues,...
                              'FaceColor','interp',varargin{:});
                    end
                end
            elseif pos.PlotConstrained == true
                for connect = pos.Connect_c                    
                    if strcmp(pos.PlotType,'mesh')
                        patch('Faces',connect.Ids,...
                            'Vertices',pos.Coords+pos.Disp*pos.MaxScale*pos.Scale,...
                            varargin{:});
                        title(pos.PlotType,'interpreter','latex');
                    else
                        if strcmp(pos.PlotType,'strain')
                            title(['$$\epsilon_{',pos.PlotAxis,'}$$'],'interpreter','latex');
                            if strcmp(pos.PlotAxis,'xx')
                                patch('Faces',connect.Ids,...
                                    'Vertices',pos.Coords+pos.Disp*pos.MaxScale*pos.Scale,...
                                    'FaceVertexCData',pos.Exx_c',...
                                    'FaceColor','interp',varargin{:});
                            elseif strcmp(pos.PlotAxis,'yy')
                                patch('Faces',connect.Ids,...
                                    'Vertices',pos.Coords+pos.Disp*pos.MaxScale*pos.Scale,...
                                    'FaceVertexCData',pos.Eyy_c',...
                                    'FaceColor','interp',varargin{:});
                            elseif strcmp(pos.PlotAxis,'xy')
                                title(['$$\gamma_{',pos.PlotAxis,'}$$'],'interpreter','latex');
                                patch('Faces',connect.Ids,...
                                    'Vertices',pos.Coords+pos.Disp*pos.MaxScale*pos.Scale,...
                                    'FaceVertexCData',pos.Gxy_c',...
                                    'FaceColor','interp',varargin{:});
                            end
                        elseif strcmp(pos.PlotType,'stress')
                            title(['$$\sigma_{',pos.PlotAxis,'}$$'],'interpreter','latex');
                            if strcmp(pos.PlotAxis,'xx')
                                patch('Faces',connect.Ids,...
                                    'Vertices',pos.Coords+pos.Disp*pos.MaxScale*pos.Scale,...
                                    'FaceVertexCData',pos.Sxx_c',...
                                    'FaceColor','interp',varargin{:});
                            elseif strcmp(pos.PlotAxis,'yy')
                                patch('Faces',connect.Ids,...
                                    'Vertices',pos.Coords+pos.Disp*pos.MaxScale*pos.Scale,...
                                    'FaceVertexCData',pos.Syy_c',...
                                    'FaceColor','interp',varargin{:});
                            elseif strcmp(pos.PlotAxis,'xy')
                                title(['$$\tau_{',pos.PlotAxis,'}$$'],'interpreter','latex');
                                patch('Faces',connect.Ids,...
                                    'Vertices',pos.Coords+pos.Disp*pos.MaxScale*pos.Scale,...
                                    'FaceVertexCData',pos.Txy_c',...
                                    'FaceColor','interp',varargin{:});
                            end
                        end
                    end
                end
%                 for connect = pos.Connect_u
%                     patch('Faces',connect.Ids,...
%                         'Vertices',pos.Coords+pos.Disp*pos.MaxScale*pos.Scale,...
%                         'FaceVertexCData',pos.Exx_u*0',...
%                         'FaceColor','interp',varargin{:});
%                 end
            elseif pos.PlotUnconstrained == true
                for connect = pos.Connect_u
                    if strcmp(pos.PlotType,'mesh')
                        patch('Faces',connect.Ids,...
                            'Vertices',pos.Coords+pos.Disp*pos.MaxScale*pos.Scale,...
                            varargin{:});
                        title(pos.PlotType,'interpreter','latex');
                    else
                        if strcmp(pos.PlotType,'strain')
                            title(['$$\epsilon_{',pos.PlotAxis,'}$$'],'interpreter','latex');
                            if strcmp(pos.PlotAxis,'xx')
                                patch('Faces',connect.Ids,...
                                    'Vertices',pos.Coords+pos.Disp*pos.MaxScale*pos.Scale,...
                                    'FaceVertexCData',pos.Exx_u',...
                                    'FaceColor','interp',varargin{:});
                            elseif strcmp(pos.PlotAxis,'yy')
                                patch('Faces',connect.Ids,...
                                    'Vertices',pos.Coords+pos.Disp*pos.MaxScale*pos.Scale,...
                                    'FaceVertexCData',pos.Eyy_u',...
                                    'FaceColor','interp',varargin{:});
                            elseif strcmp(pos.PlotAxis,'xy')
                                title(['$$\gamma_{',pos.PlotAxis,'}$$'],'interpreter','latex');
                                patch('Faces',connect.Ids,...
                                    'Vertices',pos.Coords+pos.Disp*pos.MaxScale*pos.Scale,...
                                    'FaceVertexCData',pos.Gxy_u',...
                                    'FaceColor','interp',varargin{:});
                            end
                        elseif strcmp(pos.PlotType,'stress')
                            title(['$$\sigma_{',pos.PlotAxis,'}$$'],'interpreter','latex');
                            if strcmp(pos.PlotAxis,'xx')
                                patch('Faces',connect.Ids,...
                                    'Vertices',pos.Coords+pos.Disp*pos.MaxScale*pos.Scale,...
                                    'FaceVertexCData',pos.Sxx_u',...
                                    'FaceColor','interp',varargin{:});
                            elseif strcmp(pos.PlotAxis,'yy')
                                patch('Faces',connect.Ids,...
                                    'Vertices',pos.Coords+pos.Disp*pos.MaxScale*pos.Scale,...
                                    'FaceVertexCData',pos.Syy_u',...
                                    'FaceColor','interp',varargin{:});
                            elseif strcmp(pos.PlotAxis,'xy')
                                title(['$$\tau_{',pos.PlotAxis,'}$$'],'interpreter','latex');
                                patch('Faces',connect.Ids,...
                                    'Vertices',pos.Coords+pos.Disp*pos.MaxScale*pos.Scale,...
                                    'FaceVertexCData',pos.Txy_u',...
                                    'FaceColor','interp',varargin{:});
                            end
                        end
                    end
                end
%                 for connect = pos.Connect_c
%                     patch('Faces',connect.Ids,...
%                         'Vertices',pos.Coords+pos.Disp*pos.MaxScale*pos.Scale,...
%                         'FaceVertexCData',pos.Exx_c*0',...
%                         'FaceColor','interp',varargin{:});
%                 end
            end
        end
        function set.PlotConstrained(pos,value)
            pos.PlotConstrained = value;
            if ~value
                pos.PlotUnconstrained = true; %#ok<MCSUP>
            end
        end
        function set.PlotUnconstrained(pos,value)
            pos.PlotUnconstrained = value;
            if ~value
                pos.PlotConstrained = true; %#ok<MCSUP>
            end
        end
    end
    methods (Access = private)
        
        function CalculateScale(pos)
            x_max = max(pos.Coords(:,1));
            x_min = min(pos.Coords(:,1));
            y_max = max(pos.Coords(:,2));
            y_min = min(pos.Coords(:,2));
            dx = abs(x_max - x_min);
            dy = abs(y_max - y_min);
            do = max(dx,dy);
            d = max(abs(pos.Disp));
            d = max(d(1),d(2));
            if d == 0
                pos.MaxScale = 0;
            else
                pos.MaxScale = do/d/5;
            end
        end
        
    end
end
classdef QM6 < fem.analysis.plane.Continuum2D
    methods
        function elem = QM6(nodes, prop, constitutive)
            elem@fem.analysis.plane.Continuum2D(nodes, prop, constitutive);
        end
        function [k] = Stiffness(elem)
            int = elem.Integration();
            vol = int.eval(@(x,y)elem.jacob([x,y]), 4);
            B1 = int.eval(@(x,y)elem.B1Matrix([x,y]), 4);
            B1 = B1/vol;
            k = int.eval(@(x,y)elem.StiffnessInt([x,y],B1), 4);
        end
        function k = StiffnessInt(elem,p,B1)
            [B,J] = elem.BMatrix(p);
            [Bo,Jo] = elem.B2Matrix(p,B1);
            C = elem.Constitutive();
            k11 = B'*C*B*J;
            k12 = B'*C*Bo*Jo;
            k22 = Bo'*C*Bo*Jo;
            k = [k11,k12;k12',k22];
        end
        function epsilon = CornerStrain(elem)
            int = elem.Integration();
            [x, y] = int.IntegrationPoints(int.NumPoints);
            epsilon = zeros(3,4);
            for i=1:4
                B = elem.BMatrix([x(i),y(i)]);
                epsilon(:,i) = B*[elem.Nodes.Displacement]';
            end
        end
        function [B,J] = BMatrix(elem, p)
            csi = p(1);
            eta = p(2);
            x1 = elem.Nodes(1).Coords(1);
            y1 = elem.Nodes(1).Coords(2);
            x2 = elem.Nodes(2).Coords(1);
            y2 = elem.Nodes(2).Coords(2);
            x3 = elem.Nodes(3).Coords(1);
            y3 = elem.Nodes(3).Coords(2);
            x4 = elem.Nodes(4).Coords(1);
            y4 = elem.Nodes(4).Coords(2);
            
            X = [x1,y1;
                 x2,y2;
                 x3,y3;
                 x4,y4];
            
            dN1dc=-(1-eta)/4;
            dN2dc=(1-eta)/4;
            dN3dc=(1+eta)/4;
            dN4dc=-(1+eta)/4;
            
            dN1de=-(1-csi)/4;
            dN2de=-(1+csi)/4;
            dN3de=(1+csi)/4;
            dN4de=(1-csi)/4;
            
            dNde=[dN1dc dN2dc dN3dc dN4dc;
                dN1de dN2de dN3de dN4de];
            
            J=dNde*X;
            
            dNdx=J\dNde;
            
            B = [dNdx(1,1) 0 dNdx(1,2) 0 dNdx(1,3) 0 dNdx(1,4) 0;
                0 dNdx(2,1) 0 dNdx(2,2) 0 dNdx(2,3) 0 dNdx(2,4);
                dNdx(2,1)  dNdx(1,1) dNdx(2,2) dNdx(1,2) dNdx(2,3) dNdx(1,3) dNdx(2,4) dNdx(1,4)];
            
            J = det(J)*elem.Prop.t;
        end
        function [Bo,Jo] = BoMatrix(elem, p)
            csi = p(1);
            eta = p(2);
            x1 = elem.Nodes(1).Coords(1);
            y1 = elem.Nodes(1).Coords(2);
            x2 = elem.Nodes(2).Coords(1);
            y2 = elem.Nodes(2).Coords(2);
            x3 = elem.Nodes(3).Coords(1);
            y3 = elem.Nodes(3).Coords(2);
            x4 = elem.Nodes(4).Coords(1);
            y4 = elem.Nodes(4).Coords(2);
            
            X = [x1,y1;
                 x2,y2;
                 x3,y3;
                 x4,y4];
            
            dNde=[-1 1 1 -1;
                -1 -1 1 1]/4;
            
            Jo=dNde*X;
            
            dP1dc=-2*csi;
            dP2dc=0;
            
            dP1de=0;
            dP2de=-2*eta;
            
            dPde=[dP1dc dP2dc;
                dP1de dP2de];
            
            dPdx=Jo\dPde;
            
            Bo = [dPdx(1,1) 0 dPdx(1,2) 0;
                0 dPdx(2,1) 0 dPdx(2,2);
                dPdx(2,1)  dPdx(1,1) dPdx(2,2) dPdx(1,2)];
            
            Jo = det(Jo)*elem.Prop.t;
        end
        
        function J = jacob(elem,p)
            csi = p(1);
            eta = p(2);
            x1 = elem.Nodes(1).Coords(1);
            y1 = elem.Nodes(1).Coords(2);
            x2 = elem.Nodes(2).Coords(1);
            y2 = elem.Nodes(2).Coords(2);
            x3 = elem.Nodes(3).Coords(1);
            y3 = elem.Nodes(3).Coords(2);
            x4 = elem.Nodes(4).Coords(1);
            y4 = elem.Nodes(4).Coords(2);
            
            X = [x1,y1;
                 x2,y2;
                 x3,y3;
                 x4,y4];
            
            dN1dc=-(1-eta)/4;
            dN2dc=(1-eta)/4;
            dN3dc=(1+eta)/4;
            dN4dc=-(1+eta)/4;
            
            dN1de=-(1-csi)/4;
            dN2de=-(1+csi)/4;
            dN3de=(1+csi)/4;
            dN4de=(1-csi)/4;
            
            dNde=[dN1dc dN2dc dN3dc dN4dc;
                dN1de dN2de dN3de dN4de];
            
            J=dNde*X;
            J = det(J)*elem.Prop.t;
        end
        function B = B1Matrix(elem, p)
            csi = p(1);
            eta = p(2);
            x1 = elem.Nodes(1).Coords(1);
            y1 = elem.Nodes(1).Coords(2);
            x2 = elem.Nodes(2).Coords(1);
            y2 = elem.Nodes(2).Coords(2);
            x3 = elem.Nodes(3).Coords(1);
            y3 = elem.Nodes(3).Coords(2);
            x4 = elem.Nodes(4).Coords(1);
            y4 = elem.Nodes(4).Coords(2);
            
            X = [x1,y1;
                 x2,y2;
                 x3,y3;
                 x4,y4];
            
            dN1dc=-(1-eta)/4;
            dN2dc=(1-eta)/4;
            dN3dc=(1+eta)/4;
            dN4dc=-(1+eta)/4;
            
            dN1de=-(1-csi)/4;
            dN2de=-(1+csi)/4;
            dN3de=(1+csi)/4;
            dN4de=(1-csi)/4;
            
            dNde=[dN1dc dN2dc dN3dc dN4dc;
                dN1de dN2de dN3de dN4de];
            
            J=dNde*X;
            
            dP1dc=-2*csi;
            dP2dc=0;
            
            dP1de=0;
            dP2de=-2*eta;
            
            dPde=[dP1dc dP2dc;
                dP1de dP2de];
            
            dPdx=J\dPde;
            
            B = [dPdx(1,1) 0 dPdx(1,2) 0;
                0 dPdx(2,1) 0 dPdx(2,2);
                dPdx(2,1)  dPdx(1,1) dPdx(2,2) dPdx(1,2)];
            
            J = det(J)*elem.Prop.t;
            
            B = B*J;
        end
        function [B,J] = B2Matrix(elem, p, B1)
            csi = p(1);
            eta = p(2);
            x1 = elem.Nodes(1).Coords(1);
            y1 = elem.Nodes(1).Coords(2);
            x2 = elem.Nodes(2).Coords(1);
            y2 = elem.Nodes(2).Coords(2);
            x3 = elem.Nodes(3).Coords(1);
            y3 = elem.Nodes(3).Coords(2);
            x4 = elem.Nodes(4).Coords(1);
            y4 = elem.Nodes(4).Coords(2);
            
            X = [x1,y1;
                 x2,y2;
                 x3,y3;
                 x4,y4];
            
            dN1dc=-(1-eta)/4;
            dN2dc=(1-eta)/4;
            dN3dc=(1+eta)/4;
            dN4dc=-(1+eta)/4;
            
            dN1de=-(1-csi)/4;
            dN2de=-(1+csi)/4;
            dN3de=(1+csi)/4;
            dN4de=(1-csi)/4;
            
            dNde=[dN1dc dN2dc dN3dc dN4dc;
                dN1de dN2de dN3de dN4de];
            
            J=dNde*X;
            
            dP1dc=-2*csi;
            dP2dc=0;
            
            dP1de=0;
            dP2de=-2*eta;
            
            dPde=[dP1dc dP2dc;
                dP1de dP2de];
            
            dPdx=J\dPde;
            
            B = [dPdx(1,1) 0 dPdx(1,2) 0;
                0 dPdx(2,1) 0 dPdx(2,2);
                dPdx(2,1)  dPdx(1,1) dPdx(2,2) dPdx(1,2)];
            B = B - B1;
            J = det(J)*elem.Prop.t;
        end
        function int = Integration(~)
            int = fem.integration.gauss.twoD.Quadrilateral(...
                [-1,-1],...
                [ 1,-1],...
                [ 1, 1],...
                [-1, 1]);
            int.NumPoints = 4;
        end
    end
end
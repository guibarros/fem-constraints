classdef Strain < fem.analysis.plane.ConstitutiveModel
    methods (Static)
        function C = constitutiveMatrix(E, v)
            C = E/(1+v)/(1-2*v)*[1-v    v         0;
                                   v  1-v         0;
                                   0    0 (1-2*v)/2];
        end
    end
end
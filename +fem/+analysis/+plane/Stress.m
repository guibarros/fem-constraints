classdef Stress < fem.analysis.plane.ConstitutiveModel
    methods (Static)
        function C = constitutiveMatrix(E, v)
            C = E/(1-v^2)*[1  v       0;
                           v  1       0;
                           0  0 (1-v)/2];
        end
    end
end
classdef Node < fem.node.Base
    properties (SetAccess = private)
        num_dof = 2
    end
    methods
        function no = Node(x,y)
            no@fem.node.Base([x,y]);
        end
    end
end
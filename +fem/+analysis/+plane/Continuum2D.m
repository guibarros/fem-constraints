classdef Continuum2D < fem.element.Base
    properties (SetAccess = private)
        Prop fem.analysis.plane.ElasticProp
        ConstitutiveModel fem.analysis.plane.ConstitutiveModel
    end
    methods
        function elem = Continuum2D(nodes, prop, constitutive)
            elem@fem.element.Base(nodes);
            elem.Prop = prop;
            elem.ConstitutiveModel = constitutive;
        end
        function sigma = CornerStress(elem)
            sigma = elem.Constitutive()*elem.CornerStrain();
        end
        function C = Constitutive(elem)
            C = elem.ConstitutiveModel.constitutiveMatrix(elem.Prop.E,elem.Prop.v);
        end
    end
    methods (Abstract)
        [k] = Stiffness(elem)
        [B,J] = BMatrix(elem, p)
    end
end
classdef T6 < fem.analysis.plane.Continuum2D
    methods
        function elem = T6(nodes, prop, constitutive)
            elem@fem.analysis.plane.Continuum2D(nodes, prop, constitutive);
        end
        function [k] = Stiffness(elem)
            int = elem.Integration();
            k = int.eval(@(x,y)elem.StiffnessInt([x,y]));
        end
        function k = StiffnessInt(elem,p)
            [B,J] = elem.BMatrix(p);
            C = elem.Constitutive();
            k = B'*C*B*J;
        end
        function [x,y] = CornerPoints(elem)
            x = [1,0,0,.5,0,.5];
            y = [0,1,0,.5,.5,0];
        end
        function phi = ShapeFunc(elem, p)
            csi = p(1);
            eta = p(2);            
            phi_1 = csi*(2*csi-1);
            phi_2 = eta*(2*eta-1);
            zeta = 1-csi-eta;
            phi_3 = zeta*(2*zeta-1);
            phi_4 = 4*eta*csi;
            phi_5 = 4*zeta*eta;
            phi_6 = 4*zeta*csi;            
            phi = [phi_1,phi_2,phi_3,phi_4,phi_5,phi_6];
        end
        function epsilon = CornerStrain(elem)
            B = elem.BMatrix();
            epsilon = repmat(B*[elem.Nodes.Displacement]',1,3);
        end
        function [B,J] = BMatrix(elem, p)
            csi = p(1);
            eta = p(2);
            X = zeros(6,2);
            for i=1:6
                X(i,1) = elem.Nodes(i).Coords(1);
                X(i,2) = elem.Nodes(i).Coords(2);
            end
            
            zeta = 1-csi-eta;

            dN1dc = 4*csi-1;
            dN2dc = 0;
            dN3dc = 1-4*zeta;
            dN4dc = 4*eta;
            dN5dc =-4*eta;
            dN6dc = 4*(zeta-csi);
            
            dN1de = 0;
            dN2de = 4*eta-1;
            dN3de = 1-4*zeta;
            dN4de = 4*csi;
            dN5de = 4*(zeta-eta);
            dN6de =-4*csi;
            
            dNde=[dN1dc dN2dc dN3dc dN4dc dN5dc dN6dc;
                  dN1de dN2de dN3de dN4de dN5de dN6de];
            
            J=dNde*X;
            
            dNdx=J\dNde;

            B = zeros(3, 12);
            for N=1:6
                for pdir=1:2
                    B(pdir, 2*N-2+pdir) = dNdx(pdir, N);
                    B(3, 2*N+1-pdir) = dNdx(pdir, N);
                end
            end
            
            J = det(J)*elem.Prop.t;
        end
        function int = Integration(elem)
            int = fem.integration.gauss.twoD.Triangle(...
                [1, 0],...
                [0, 1],...
                [0, 0]);
            int.NumPoints = 4;
        end
    end
end
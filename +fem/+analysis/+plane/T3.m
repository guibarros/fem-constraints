classdef T3 < fem.analysis.plane.Continuum2D
    methods
        function elem = T3(nodes, prop, constitutive)
            elem@fem.analysis.plane.Continuum2D(nodes, prop, constitutive);
        end
        function [k] = Stiffness(elem)
            int = elem.Integration();
            k = int.eval(@(x,y)elem.StiffnessInt([x,y]), int.NumPoints);
        end
        function k = StiffnessInt(elem,p)
            [B,J] = elem.BMatrix(p);
            C = elem.Constitutive();
            k = B'*C*B*J;
        end
        function [x,y] = CornerPoints(elem)
            x = [1,0,0];
            y = [0,1,0];
        end
        function phi = ShapeFunc(elem, p)
            csi = p(1);
            eta = p(2);            
            phi_1 = csi;
            phi_2 = eta;
            phi_3 = 1-csi-eta;
            
            phi = [phi_1,phi_2,phi_3];
        end
        function epsilon = CornerStrain(elem)
            B = elem.BMatrix();
            epsilon = repmat(B*[elem.Nodes.Displacement]',1,3);
        end
        function [B,J] = BMatrix(elem, ~)
            x1 = elem.Nodes(1).Coords(1);
            y1 = elem.Nodes(1).Coords(2);
            x2 = elem.Nodes(2).Coords(1);
            y2 = elem.Nodes(2).Coords(2);
            x3 = elem.Nodes(3).Coords(1);
            y3 = elem.Nodes(3).Coords(2);
            
            X = [x1,y1;
                 x2,y2;
                 x3,y3];
            
            dN1dc = 1;
            dN2dc = 0;
            dN3dc = -1;
            
            dN1de = 0;
            dN2de = 1;
            dN3de =-1;
            
            dNde=[dN1dc dN2dc dN3dc;
                  dN1de dN2de dN3de];
            
            J=dNde*X;
            
            dNdx=J\dNde;
            
            B = [dNdx(1,1) 0 dNdx(1,2) 0 dNdx(1,3) 0;
                0 dNdx(2,1) 0 dNdx(2,2) 0 dNdx(2,3);
                dNdx(2,1)  dNdx(1,1) dNdx(2,2) dNdx(1,2) dNdx(2,3) dNdx(1,3)];
            
            J = det(J)*elem.Prop.t;
        end
        function int = Integration(elem)
            int = fem.integration.gauss.twoD.Triangle(...
                [1, 0],...
                [0, 1],...
                [0, 0]);
            int.NumPoints = 1;
        end
    end
end
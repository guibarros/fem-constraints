classdef Q8 < fem.analysis.plane.Continuum2D
    methods
        function elem = Q8(nodes, prop, constitutive)
            elem@fem.analysis.plane.Continuum2D(nodes, prop, constitutive);
        end
        function [k] = Stiffness(elem)
            int = elem.Integration();
            k = int.eval(@(x,y)elem.StiffnessInt([x,y]));
        end
        function k = StiffnessInt(elem,p)
            [B,J] = elem.BMatrix(p);
            C = elem.Constitutive();
            k = B'*C*B*J;
        end
        function epsilon = CornerStrain(elem)
            int = elem.Integration();
            [x, y] = int.IntegrationPoints(int.NumPoints);
            epsilon = zeros(3,int.NumPoints);
            for i=1:int.NumPoints
                B = elem.BMatrix([x(i),y(i)]);
                epsilon(:,i) = B*[elem.Nodes.Displacement]';
            end
            exx = fit([x',y'],epsilon(1,:)','poly11');
            eyy = fit([x',y'],epsilon(2,:)','poly11');
            gxy = fit([x',y'],epsilon(3,:)','poly11');
            [x,y] = elem.CornerPoints();
            epsilon = [exx(x,y);eyy(x,y);gxy(x,y)];
        end
        function [x,y] = CornerPoints(elem)
            x = [-1,1,1,-1,0,1,0,-1];
            y = [-1,-1,1,1,-1,0,1,0];
        end
%         function sigma = CornerStress(elem)
%             int = elem.Integration();
%             [x, y] = int.IntegrationPoints(int.NumPoints);
%             epsilon = zeros(3,int.NumPoints);
%             for i=1:int.NumPoints
%                 B = elem.BMatrix([x(i),y(i)]);
%                 epsilon(:,i) = B*[elem.Nodes.Displacement]';
%             end
%             sigma = elem.Constitutive()*epsilon;
%             sxx = fit([x',y'],sigma(1,:)','poly11');
%             syy = fit([x',y'],sigma(2,:)','poly11');
%             txy = fit([x',y'],sigma(3,:)','poly11');
%             x = [-1,1,1,-1,0,1,0,-1];
%             y = [-1,-1,1,1,-1,0,1,0];
%             sigma = [sxx(x,y);syy(x,y);txy(x,y)];
%         end
        function phi = ShapeFunc(elem, p)
            csi = p(1);
            eta = p(2);
            
            phi_5=(1-csi^2)*(1-eta)/2;
            phi_6=(1+csi)*(1-eta^2)/2;
            phi_7=(1-csi^2)*(eta+1)/2;
            phi_8=(1-csi)*(1-eta^2)/2;
            phi_1=(1-csi)*(1-eta)/4-phi_5/2-phi_8/2;
            phi_2=(1+csi)*(1-eta)/4-phi_5/2-phi_6/2;
            phi_3=(1+csi)*(1+eta)/4-phi_6/2-phi_7/2;
            phi_4=(1-csi)*(1+eta)/4-phi_7/2-phi_8/2;
            
            phi = [phi_1,phi_2,phi_3,phi_4,phi_5,phi_6,phi_7,phi_8];
        end
        function [B,J] = BMatrix(elem, p)
            csi = p(1);
            eta = p(2);
            X = zeros(8,2);
            for i=1:8
                X(i,1) = elem.Nodes(i).Coords(1);
                X(i,2) = elem.Nodes(i).Coords(2);
            end

            dN1dc= eta/4 - csi*(eta-1)/2 - eta^2/4;
            dN2dc= eta^2/4 - csi*(eta-1)/2 - eta/4;
            dN3dc= eta/4 + csi*(eta+1)/2 + eta^2/4;
            dN4dc= csi*(eta+1)/2 - eta/4 - eta^2/4;
            dN5dc= csi*(eta-1);
            dN6dc= (1-eta^2)/2;
            dN7dc= -csi*(eta+1);
            dN8dc= (eta^2-1)/2;

            dN1de= csi/4 - csi^2/4 - eta*(csi-1)/2;
            dN2de= eta*(csi+1)/2 - csi^2/4 - csi/4;
            dN3de= csi/4 + csi^2/4 + eta*(csi+1)/2;
            dN4de= csi^2/4 - csi/4 - eta*(csi-1)/2;
            dN5de= (csi^2-1)/2;
            dN6de= -eta*(csi+1);
            dN7de= (1-csi^2)/2;
            dN8de= eta*(csi-1);

            dNde=[dN1dc dN2dc dN3dc dN4dc dN5dc dN6dc dN7dc dN8dc;
                  dN1de dN2de dN3de dN4de dN5de dN6de dN7de dN8de];
            
            J=dNde*X;
            
            dNdx=J\dNde;
            
            B = zeros(3, 16);
            for N=1:8
                for pdir=1:2
                    B(pdir, 2*N-2+pdir) = dNdx(pdir, N);
                    B(3, 2*N+1-pdir) = dNdx(pdir, N);
                end
            end

            J = det(J)*elem.Prop.t;
        end
        function [x,y] = coordinates(elem, p)
            x1 = elem.Nodes(1).Coords(1);
            y1 = elem.Nodes(1).Coords(2);
            x2 = elem.Nodes(2).Coords(1);
            y2 = elem.Nodes(2).Coords(2);
            x3 = elem.Nodes(3).Coords(1);
            y3 = elem.Nodes(3).Coords(2);
            x4 = elem.Nodes(4).Coords(1);
            y4 = elem.Nodes(4).Coords(2);
            x5 = elem.Nodes(5).Coords(1);
            y5 = elem.Nodes(5).Coords(2);
            x6 = elem.Nodes(6).Coords(1);
            y6 = elem.Nodes(6).Coords(2);
            x7 = elem.Nodes(7).Coords(1);
            y7 = elem.Nodes(7).Coords(2);
            x8 = elem.Nodes(8).Coords(1);
            y8 = elem.Nodes(8).Coords(2);
            
            X = [x1,y1;
                 x2,y2;
                 x3,y3;
                 x4,y4;
                 x5,y5;
                 x6,y6;
                 x7,y7;
                 x8,y8];
            
            N = elem.ShapeFunc(p);
            
            X = N*X;
            
            x = X(1);
            y = X(2);
            
        end
        function int = Integration(~)
            int = fem.integration.gauss.twoD.Quadrilateral(...
                [-1,-1],...
                [ 1,-1],...
                [ 1, 1],...
                [-1, 1]);
            int.NumPoints = 4;
        end
    end
end
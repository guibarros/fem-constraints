classdef Navier < fem.element.Base
    properties (SetAccess = private)
        Prop fem.analysis.frame.NavierProp
    end
    methods
        function elem = Navier(nodes, prop)
            elem@fem.element.Base(nodes);
            if length(elem.Nodes) == 2
                elem.Prop = prop;
            else
                error('Navier element must have only two nodes.');
            end
        end
        function k = Stiffness(elem)
            [B,J] = elem.BMatrix();
            k = B'*elem.Constitutive()*B*J;
        end
        function epsilon = CornerStrain(elem)
            B = elem.BMatrix();
            epsilon = B*[elem.Nodes.Displacement]';
        end
        function sigma = CornerStress(elem)
            sigma = elem.Constitutive()*elem.CornerStrain();
        end
        function [k] = Constitutive(elem)
            k = 2*elem.Prop.EI*[2,-1;-1,2];
            O = zeros(1,2);
            k = [elem.Prop.EA,O;
                O',k];
        end
        function [B,J] = BMatrix(elem, ~)
            x1 = elem.Nodes(1).Coords(1);
            y1 = elem.Nodes(1).Coords(2);
            x2 = elem.Nodes(2).Coords(1);
            y2 = elem.Nodes(2).Coords(2);
            
            dx = x2-x1;
            dy = y2-y1;
            
            L = sqrt(dx*dx+dy*dy);
            
            B = [-1,   0, 0,1,   0,0;
                  0,-1/L,-1,0, 1/L,0;
                  0, 1/L, 0,0,-1/L,1];
            
            R = fem.analysis.frame.Navier.rotation(dx,dy,L);
            
            B = B*R;
            
            J = 1/L;
        end
        function int = Integration(elem)
            x1 = elem.Nodes(1).Coords(1);
            y1 = elem.Nodes(1).Coords(2);
            x2 = elem.Nodes(2).Coords(1);
            y2 = elem.Nodes(2).Coords(2);
            
            dx = x2-x1;
            dy = y2-y1;
            
            L = sqrt(dx*dx+dy*dy);
            int = fem.integration.gauss.oneD.Interval(0,L);
            int.NumPoints = 5;
        end
    end
    methods (Static)
        function R = rotation(dx,dy,L)
            c = dx/L;
            s = dy/L;
            R = [ c  s  0  0  0  0;
                -s  c  0  0  0  0;
                0  0  1  0  0  0;
                0  0  0  c  s  0;
                0  0  0 -s  c  0;
                0  0  0  0  0  1];
        end
    end
end
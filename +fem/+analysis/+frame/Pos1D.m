classdef Pos1D < handle
    properties (SetAccess = private)
        Elements  fem.element.Base = fem.element.Base.empty
        Nodes     fem.analysis.frame.Node = fem.analysis.frame.Node.empty
    end
    
    properties (Access = public)
        PlotType = 'deformed'
        MaxScale = 0.0
        Scale = 1.0
    end
    methods (Access = public)
        function pos = Pos1D(nodes,elements)
            pos.Nodes = nodes;
            pos.Elements = elements;
        end
        function plot(pos,varargin)
            if pos.MaxScale == 0.0
                pos.CalculateScale;
            end
            hold on
            for el = pos.Elements
                x1 = el.Nodes(1).Coords(1);
                y1 = el.Nodes(1).Coords(2);
                x2 = el.Nodes(2).Coords(1);
                y2 = el.Nodes(2).Coords(2);
                dx = x2-x1;
                dy = y2-y1;
                L = sqrt(dx*dx+dy*dy);
                c = dx/L;
                s = dy/L;
                R = [ c  s  0  0  0  0;
                     -s  c  0  0  0  0;
                      0  0  1  0  0  0;
                      0  0  0  c  s  0;
                      0  0  0 -s  c  0;
                      0  0  0  0  0  1];
                d = R*[el.Nodes.Displacement]';
                u1 = d(1);
                v1 = d(2);
                t1 = d(3);
                u2 = d(4);
                v2 = d(5);
                t2 = d(6);               
                
                t = linspace(0,L,1000);
                u = (1-t/L)*u1 + (t/L)*u2;
                N1 = 1 - 3*(t/L).^2 + 2*(t/L).^3;
                N2 = 3*(t/L).^2 - 2*(t/L).^3;
                N3 = t.*((1-t/L).^2);
                N4 = t.*((t/L).^2 - t/L);
                v = N1*v1 + N2*v2 + N3*t1 + N4*t2;
                
                u = R(1:2,1:2)'*[u;v];
                v = u(2,:);
                u = u(1,:);
                
                x = (1-t/L)*x1 + (t/L)*x2;
                y = (1-t/L)*y1 + (t/L)*y2;
                x = x + u*pos.MaxScale*pos.Scale;
                y = y + v*pos.MaxScale*pos.Scale;
                plot(x,y,varargin{:})
            end
            hold off
        end
    end
    methods (Access = private)
        
        function CalculateScale(pos)
            coords = reshape([pos.Nodes.Coords],2,length(pos.Nodes))';
            x_max = max(coords(:,1));
            x_min = min(coords(:,1));
            y_max = max(coords(:,2));
            y_min = min(coords(:,2));
            dx = abs(x_max - x_min);
            dy = abs(y_max - y_min);
            do = max(dx,dy);
            d = 0;
            for el = pos.Elements
                x1 = el.Nodes(1).Coords(1);
                y1 = el.Nodes(1).Coords(2);
                x2 = el.Nodes(2).Coords(1);
                y2 = el.Nodes(2).Coords(2);
                dx = x2-x1;
                dy = y2-y1;
                L = sqrt(dx*dx+dy*dy);
                c = dx/L;
                s = dy/L;
                R = [ c  s  0  0  0  0;
                     -s  c  0  0  0  0;
                      0  0  1  0  0  0;
                      0  0  0  c  s  0;
                      0  0  0 -s  c  0;
                      0  0  0  0  0  1];
                u = R*[el.Nodes.Displacement]';
                u1 = u(1);
                v1 = u(2);
                t1 = u(3);
                u2 = u(4);
                v2 = u(5);
                t2 = u(6);               
                
                t = linspace(0,L,1000);
                u = (1-t/L)*u1 + (t/L)*u2;
                N1 = 1 - 3*(t/L).^2 + 2*(t/L).^3;
                N2 = 3*(t/L).^2 - 2*(t/L).^3;
                N3 = t.*((1-t/L).^2);
                N4 = t.*((t/L).^2 - t/L);
                v = N1*v1 + N2*v2 + N3*t1 + N4*t2;
                
                u = R(1:2,1:2)'*[u;v];
                
                d_trial = max(max(abs(u')));
                if d_trial > d
                    d = d_trial;
                end
            end
            pos.MaxScale = do/d/5;
        end
        
    end
end
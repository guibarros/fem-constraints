classdef NavierProp < fem.element.property.Base
    properties (SetAccess = private)
        EA(1,1) double
        EI(1,1) double
    end
    methods
        function prop = NavierProp(EA, EI)
            prop.EA = EA;
            prop.EI = EI;
        end
    end
end
classdef Default < fem.node.Base
    properties (SetAccess = private)
        num_dof = 0
    end
    methods
        function no = Default()
            no@fem.node.Base(double.empty);
        end
    end
end
classdef Base < handle & matlab.mixin.Heterogeneous & matlab.mixin.Copyable
    properties (SetAccess = private)
        Coords double = double.empty
        BC fem.node.BoundaryCondition = fem.node.BoundaryCondition.empty
    end
    properties (SetAccess = ?fem.Analysis)
        Displacement double = double.empty
        Force        double = double.empty
    end
    properties (SetAccess = private, Abstract)
        num_dof
    end
    properties (Hidden)
        id
    end
    methods
        function no = Base(coords)
            no.Coords = coords;
            no.Displacement = zeros(1,no.num_dof);
            no.Force        = zeros(1,no.num_dof);
        end
        function setDisplacements(no, dof, displacements_values)
            if isempty(no.BC)
                forces_dof = [];
            else
                forces_dof = no.BC.Force();
            end
            no.BC = fem.node.BoundaryCondition(dof, forces_dof);
            no.Displacement(dof) = displacements_values;
        end
        function setForces(no, dof, forces_values)
            if isempty(no.BC)
                displacements_dof = [];
            else
                displacements_dof = no.BC.Displacement();
            end
            no.BC = fem.node.BoundaryCondition(displacements_dof, dof);
            no.Force(dof) = forces_values;
        end
    end
    methods (Static, Sealed, Access = protected)
        function defaultObject = getDefaultScalarElement
            defaultObject = fem.node.Default;
        end
    end
end
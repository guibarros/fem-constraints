classdef BoundaryCondition < handle
    properties (SetAccess = private)
        Force
        Displacement
    end
    methods
        function no = BoundaryCondition(...
                displacements_dof,...
                forces_dof)
            if ~isempty(intersect(displacements_dof,forces_dof))
                error('A degree of freedom cannot have both force and displacement specified.');
            end
            no.Force = forces_dof;
            no.Displacement = displacements_dof;
        end
    end
end